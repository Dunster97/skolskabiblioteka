USE [SkolskaBiblioteka]
GO
/****** Object:  Table [dbo].[Autor]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Autor](
	[AutorID] [int] IDENTITY(1,1) NOT NULL,
	[Ime] [nvarchar](50) NOT NULL,
	[Prezime] [nvarchar](50) NOT NULL,
	[DatumRodjenja] [date] NULL,
	[Adresa] [nvarchar](100) NULL,
 CONSTRAINT [PK_Autor] PRIMARY KEY CLUSTERED 
(
	[AutorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Grupa]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grupa](
	[GrupaID] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [nvarchar](50) NOT NULL,
	[AccessLevel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[GrupaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Izdavac]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Izdavac](
	[IzdavacID] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Izdavac] PRIMARY KEY CLUSTERED 
(
	[IzdavacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Knjiga]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Knjiga](
	[KnjigaID] [int] IDENTITY(1,1) NOT NULL,
	[UDK] [nvarchar](50) NULL,
	[ISBN] [nvarchar](50) NULL,
	[Naziv] [nvarchar](100) NOT NULL,
	[SignaturaID] [int] NULL,
	[PicPath] [nvarchar](max) NULL,
	[IzdavacID] [int] NULL,
	[DatumIzdavanja] [date] NULL,
	[PDFPath] [nvarchar](max) NULL,
	[PapirnoIzdanje] [bit] NULL,
	[Opis] [nvarchar](max) NULL,
 CONSTRAINT [PK_Knjiga] PRIMARY KEY CLUSTERED 
(
	[KnjigaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Komentar]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Komentar](
	[KomentarID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NOT NULL,
	[KnjigaID] [int] NOT NULL,
	[VremeKomentara] [datetime] NOT NULL,
	[Reported] [bit] NOT NULL,
	[KomentarText] [nvarchar](max) NULL,
	[VoteUp] [int] NULL,
	[VoteDown] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[KomentarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Korisnik]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Korisnik](
	[KorisnikID] [int] IDENTITY(1,1) NOT NULL,
	[MaticniBroj] [nvarchar](50) NOT NULL,
	[Ime] [nvarchar](50) NOT NULL,
	[Prezime] [nvarchar](50) NOT NULL,
	[Mesto] [nvarchar](50) NULL,
	[Adresa] [nvarchar](100) NULL,
	[Telefon] [varchar](20) NULL,
	[ClanskaKartaBroj] [nvarchar](10) NOT NULL,
	[BarCode] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](max) NULL,
	[PasswordSalt] [nvarchar](max) NULL,
	[PicPath] [nvarchar](max) NULL,
	[LoginAttempt] [int] NULL,
	[LockedDateTime] [datetime] NULL,
	[ConfirmPassword] [nvarchar](max) NULL,
	[LatestVisit] [datetime] NULL,
 CONSTRAINT [PK_Korisnik] PRIMARY KEY CLUSTERED 
(
	[KorisnikID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KorisnikGrupa]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KorisnikGrupa](
	[KorisnikID] [int] NOT NULL,
	[GrupaID] [int] NOT NULL,
	[DatumDodavanja] [date] NOT NULL,
 CONSTRAINT [PK_KorisnikGrupa] PRIMARY KEY CLUSTERED 
(
	[KorisnikID] ASC,
	[GrupaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Na_Citanju]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Na_Citanju](
	[Na_CitanjuID] [int] IDENTITY(1,1) NOT NULL,
	[PrimerakID] [int] NOT NULL,
	[KorisnikID] [int] NOT NULL,
	[DatumUzimanja] [date] NOT NULL,
	[DatumVracanja] [date] NULL,
	[RokZaVracanje] [date] NULL,
 CONSTRAINT [PK_Na_Citanju] PRIMARY KEY CLUSTERED 
(
	[Na_CitanjuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Napisali]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Napisali](
	[AutorID] [int] NOT NULL,
	[KnjigaID] [int] NOT NULL,
 CONSTRAINT [PK_Napisali] PRIMARY KEY CLUSTERED 
(
	[AutorID] ASC,
	[KnjigaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Obavestenje]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Obavestenje](
	[ObavestenjeID] [int] IDENTITY(1,1) NOT NULL,
	[Kreator] [nvarchar](100) NULL,
	[Tekst] [nvarchar](max) NOT NULL,
	[PostDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ObavestenjeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ObavestenjeKorisnik]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ObavestenjeKorisnik](
	[ObavestenjeKorisnikID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NOT NULL,
	[ObavestenjeID] [int] NOT NULL,
	[SeenDate] [datetime] NULL,
	[Seen] [bit] NULL,
 CONSTRAINT [PK__Obaveste__881419C7F420EB35] PRIMARY KEY CLUSTERED 
(
	[ObavestenjeKorisnikID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ocena]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ocena](
	[OcenaID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NULL,
	[KnjigaID] [int] NULL,
	[Ocena] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OcenaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Povezi]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Povezi](
	[VrstaPovezaID] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [nvarchar](50) NULL,
 CONSTRAINT [PK_Povezi] PRIMARY KEY CLUSTERED 
(
	[VrstaPovezaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Preuzeo]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preuzeo](
	[PreuzeoID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NOT NULL,
	[KnjigaID] [int] NOT NULL,
	[VremePreuzimanja] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PreuzeoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PrijavaKomentara]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrijavaKomentara](
	[PrijavaKomentaraId] [int] IDENTITY(1,1) NOT NULL,
	[KomentarId] [int] NULL,
	[KorisnikId] [int] NULL,
	[Tekst] [nvarchar](max) NULL,
	[VremePrijave] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PrijavaKomentaraId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Primerak]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Primerak](
	[PrimerakID] [int] IDENTITY(1,1) NOT NULL,
	[KnjigaID] [int] NOT NULL,
	[InventarskiBroj] [nvarchar](50) NOT NULL,
	[Aktivna] [bit] NOT NULL DEFAULT ((1)),
	[Depo] [bit] NULL,
	[VrstaPovezaID] [int] NULL,
	[BarCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Primerak] PRIMARY KEY CLUSTERED 
(
	[PrimerakID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rezervacije]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rezervacije](
	[RezervacijeID] [int] IDENTITY(1,1) NOT NULL,
	[KnjigaID] [int] NOT NULL,
	[KorisnikID] [int] NOT NULL,
	[DatumPocetkaRezervacije] [date] NOT NULL,
	[DatumIstekaRezervacije] [date] NOT NULL,
	[Validna] [bit] NOT NULL,
	[PrimerakID] [int] NULL,
 CONSTRAINT [PK_Rezervacije_1] PRIMARY KEY CLUSTERED 
(
	[RezervacijeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Signatura]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Signatura](
	[SignaturaID] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [nvarchar](50) NULL,
 CONSTRAINT [PK_Signatura] PRIMARY KEY CLUSTERED 
(
	[SignaturaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteDown]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteDown](
	[VoteDownID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NULL,
	[KomentarID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VoteDownID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteUp]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteUp](
	[VoteUpID] [int] IDENTITY(1,1) NOT NULL,
	[KorisnikID] [int] NULL,
	[KomentarID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VoteUpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Autor] ON 

INSERT [dbo].[Autor] ([AutorID], [Ime], [Prezime], [DatumRodjenja], [Adresa]) VALUES (1, N'Ivo', N'Andric', NULL, NULL)
INSERT [dbo].[Autor] ([AutorID], [Ime], [Prezime], [DatumRodjenja], [Adresa]) VALUES (2, N'Dobrica', N'Ćosić', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Autor] OFF
SET IDENTITY_INSERT [dbo].[Grupa] ON 

INSERT [dbo].[Grupa] ([GrupaID], [Naziv], [AccessLevel]) VALUES (1, N'Citalac', 1)
INSERT [dbo].[Grupa] ([GrupaID], [Naziv], [AccessLevel]) VALUES (2, N'Administrator', 7)
SET IDENTITY_INSERT [dbo].[Grupa] OFF
SET IDENTITY_INSERT [dbo].[Izdavac] ON 

INSERT [dbo].[Izdavac] ([IzdavacID], [Naziv]) VALUES (1, N'Vulkan')
INSERT [dbo].[Izdavac] ([IzdavacID], [Naziv]) VALUES (2, N'Gutenbergova galaksija')
SET IDENTITY_INSERT [dbo].[Izdavac] OFF
SET IDENTITY_INSERT [dbo].[Knjiga] ON 

INSERT [dbo].[Knjiga] ([KnjigaID], [UDK], [ISBN], [Naziv], [SignaturaID], [PicPath], [IzdavacID], [DatumIzdavanja], [PDFPath], [PapirnoIzdanje], [Opis]) VALUES (1, NULL, NULL, N'Na drini ćuprija', 2, N'http://www.smrtnakazna.rs/portals/0/Galerija/Ivo-Andric-Na-Drini-cuprija-211x300.jpg', 1, NULL, NULL, 1, N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet lobortis arcu. Aliquam non condimentum mauris, vitae ornare dolor. Integer sed blandit tellus. Duis porta dignissim lacus, eget commodo nisl fermentum a. Aenean commodo volutpat ligula, sit amet dapibus purus pretium sed. Fusce sed iaculis dui. Praesent ultricies, ante eget congue sollicitudin, sapien neque porttitor sem, in eleifend magna nisl quis tellus. Etiam iaculis a quam eu fringilla. Curabitur placerat sem tellus, at tincidunt nisl aliquet id. Nunc id tellus vel dui efficitur bibendum ut ac eros. Donec vitae porttitor eros. Ut gravida diam at magna cursus, quis posuere nulla elementum.')
INSERT [dbo].[Knjiga] ([KnjigaID], [UDK], [ISBN], [Naziv], [SignaturaID], [PicPath], [IzdavacID], [DatumIzdavanja], [PDFPath], [PapirnoIzdanje], [Opis]) VALUES (2, N'asdf', N'asdf', N'Vreme smrti', 2, N'http://i.gr-assets.com/images/S/photo.goodreads.com/books/1402049916i/22433022._UY200_.jpg', 2, NULL, N'http://qwerty', 1, NULL)
SET IDENTITY_INSERT [dbo].[Knjiga] OFF
SET IDENTITY_INSERT [dbo].[Komentar] ON 

INSERT [dbo].[Komentar] ([KomentarID], [KorisnikID], [KnjigaID], [VremeKomentara], [Reported], [KomentarText], [VoteUp], [VoteDown]) VALUES (1, 3, 1, CAST(N'2016-05-06 20:34:18.503' AS DateTime), 1, N'Preporučio bih svima da pročitaju ovu knjigu.', 1, 0)
INSERT [dbo].[Komentar] ([KomentarID], [KorisnikID], [KnjigaID], [VremeKomentara], [Reported], [KomentarText], [VoteUp], [VoteDown]) VALUES (2, 3, 1, CAST(N'2016-05-06 20:34:24.283' AS DateTime), 0, N'Preporučio bih svima da pročitaju ovu knjigu.', 1, 0)
INSERT [dbo].[Komentar] ([KomentarID], [KorisnikID], [KnjigaID], [VremeKomentara], [Reported], [KomentarText], [VoteUp], [VoteDown]) VALUES (3, 2, 2, CAST(N'2016-05-09 08:33:22.617' AS DateTime), 0, N'asdfhtfythchgfjhfjfhfh', 0, 0)
SET IDENTITY_INSERT [dbo].[Komentar] OFF
SET IDENTITY_INSERT [dbo].[Korisnik] ON 

INSERT [dbo].[Korisnik] ([KorisnikID], [MaticniBroj], [Ime], [Prezime], [Mesto], [Adresa], [Telefon], [ClanskaKartaBroj], [BarCode], [Email], [Username], [Password], [PasswordSalt], [PicPath], [LoginAttempt], [LockedDateTime], [ConfirmPassword], [LatestVisit]) VALUES (2, N'1234656123456', N'admin', N'admin', N'admin', N'admin', N'admin', N'0000000016', NULL, N'admin@admin.admin', N'admin', N'kxRIud2pf3xZ96f5H6JtLnoDHE3iBW2ROesVSxFj52plEK3utnfGu/vd1SEJPcVPAV9LbFyS8F+h6GkZtZvYoA==', N'100000.Jhg8Nkl1rKFfsPwWounu+pgg41+1L33xZcsy8dfhzLtWXA==', N'/Images/avatar.png', 0, CAST(N'2011-05-06 20:16:57.700' AS DateTime), N'kxRIud2pf3xZ96f5H6JtLnoDHE3iBW2ROesVSxFj52plEK3utnfGu/vd1SEJPcVPAV9LbFyS8F+h6GkZtZvYoA==', CAST(N'2016-05-09 08:32:48.180' AS DateTime))
INSERT [dbo].[Korisnik] ([KorisnikID], [MaticniBroj], [Ime], [Prezime], [Mesto], [Adresa], [Telefon], [ClanskaKartaBroj], [BarCode], [Email], [Username], [Password], [PasswordSalt], [PicPath], [LoginAttempt], [LockedDateTime], [ConfirmPassword], [LatestVisit]) VALUES (3, N'1234567891234', N'Stefan', N'Petković', NULL, NULL, NULL, N'0000000116', NULL, N'stefan@stefan.stefan', N'dunster', N'5Zc1T51VVSRWr4lBXVpYPNQLXu+DDNIKKG3a88Dm4jv1cLYVOBQeFtKJRkeg//bV+ipdShHvcSbGfBYDjOdDNw==', N'100000.89AKpPyX4ACL4ai+d1D95OH2htOo2morA5WbQOZlLELM7g==', N'/Images/UserImages/a7848437e48a1eb92ba30dd3eacc0fd3cce086b7_full.jpg', 0, CAST(N'2011-05-06 20:29:14.977' AS DateTime), N'5Zc1T51VVSRWr4lBXVpYPNQLXu+DDNIKKG3a88Dm4jv1cLYVOBQeFtKJRkeg//bV+ipdShHvcSbGfBYDjOdDNw==', CAST(N'2016-05-09 08:34:19.367' AS DateTime))
INSERT [dbo].[Korisnik] ([KorisnikID], [MaticniBroj], [Ime], [Prezime], [Mesto], [Adresa], [Telefon], [ClanskaKartaBroj], [BarCode], [Email], [Username], [Password], [PasswordSalt], [PicPath], [LoginAttempt], [LockedDateTime], [ConfirmPassword], [LatestVisit]) VALUES (4, N'0101997740018', N'Darko', N'Mitic', N'Nis', N'Radnih brigada 9/24', N'0693194805', N'0000000216', NULL, N'tdm.mail97@gmail.com', N'darkomtc', N'kES1y5HZiT0CsdVOOWKYNSmKWdZkKHfvCmgYWrKq3j9AVfatvSR+6/S1OpLM74FF0yQg9asWqnpRQtTRi0qRaQ==', N'100000.LSTFjxkp6qxfogLkDBzBd6kcAA+TIU0vBu3xoQglnQqGUw==', N'/Images/avatar.png', 0, CAST(N'2011-05-07 23:16:29.923' AS DateTime), N'kES1y5HZiT0CsdVOOWKYNSmKWdZkKHfvCmgYWrKq3j9AVfatvSR+6/S1OpLM74FF0yQg9asWqnpRQtTRi0qRaQ==', CAST(N'2016-05-07 23:16:39.420' AS DateTime))
SET IDENTITY_INSERT [dbo].[Korisnik] OFF
INSERT [dbo].[KorisnikGrupa] ([KorisnikID], [GrupaID], [DatumDodavanja]) VALUES (2, 2, CAST(N'2016-05-06' AS Date))
INSERT [dbo].[KorisnikGrupa] ([KorisnikID], [GrupaID], [DatumDodavanja]) VALUES (3, 1, CAST(N'2016-05-06' AS Date))
INSERT [dbo].[KorisnikGrupa] ([KorisnikID], [GrupaID], [DatumDodavanja]) VALUES (4, 1, CAST(N'2016-05-07' AS Date))
SET IDENTITY_INSERT [dbo].[Na_Citanju] ON 

INSERT [dbo].[Na_Citanju] ([Na_CitanjuID], [PrimerakID], [KorisnikID], [DatumUzimanja], [DatumVracanja], [RokZaVracanje]) VALUES (1, 1, 3, CAST(N'2016-05-06' AS Date), NULL, CAST(N'2016-05-27' AS Date))
SET IDENTITY_INSERT [dbo].[Na_Citanju] OFF
INSERT [dbo].[Napisali] ([AutorID], [KnjigaID]) VALUES (2, 2)
SET IDENTITY_INSERT [dbo].[Povezi] ON 

INSERT [dbo].[Povezi] ([VrstaPovezaID], [Naziv]) VALUES (1, N'Meki')
INSERT [dbo].[Povezi] ([VrstaPovezaID], [Naziv]) VALUES (2, N'Tvrdi')
SET IDENTITY_INSERT [dbo].[Povezi] OFF
SET IDENTITY_INSERT [dbo].[Preuzeo] ON 

INSERT [dbo].[Preuzeo] ([PreuzeoID], [KorisnikID], [KnjigaID], [VremePreuzimanja]) VALUES (1, 3, 1, CAST(N'2016-05-06 20:33:45.560' AS DateTime))
INSERT [dbo].[Preuzeo] ([PreuzeoID], [KorisnikID], [KnjigaID], [VremePreuzimanja]) VALUES (2, 2, 1, CAST(N'2016-05-07 23:14:08.650' AS DateTime))
SET IDENTITY_INSERT [dbo].[Preuzeo] OFF
SET IDENTITY_INSERT [dbo].[PrijavaKomentara] ON 

INSERT [dbo].[PrijavaKomentara] ([PrijavaKomentaraId], [KomentarId], [KorisnikId], [Tekst], [VremePrijave]) VALUES (1, 1, 2, N'aiudsfsdbfsdfsidhfidshfdshiufuhdsiufhiusdhfiudhsfiuhdsiufhsdiufhidsuhfiu', CAST(N'2016-05-07 22:09:29.110' AS DateTime))
SET IDENTITY_INSERT [dbo].[PrijavaKomentara] OFF
SET IDENTITY_INSERT [dbo].[Primerak] ON 

INSERT [dbo].[Primerak] ([PrimerakID], [KnjigaID], [InventarskiBroj], [Aktivna], [Depo], [VrstaPovezaID], [BarCode]) VALUES (1, 1, N'123456', 0, 1, 1, NULL)
INSERT [dbo].[Primerak] ([PrimerakID], [KnjigaID], [InventarskiBroj], [Aktivna], [Depo], [VrstaPovezaID], [BarCode]) VALUES (2, 1, N'1234567', 0, 0, 2, NULL)
SET IDENTITY_INSERT [dbo].[Primerak] OFF
SET IDENTITY_INSERT [dbo].[Rezervacije] ON 

INSERT [dbo].[Rezervacije] ([RezervacijeID], [KnjigaID], [KorisnikID], [DatumPocetkaRezervacije], [DatumIstekaRezervacije], [Validna], [PrimerakID]) VALUES (3, 1, 3, CAST(N'2016-05-07' AS Date), CAST(N'2016-05-12' AS Date), 1, 1)
INSERT [dbo].[Rezervacije] ([RezervacijeID], [KnjigaID], [KorisnikID], [DatumPocetkaRezervacije], [DatumIstekaRezervacije], [Validna], [PrimerakID]) VALUES (8, 1, 2, CAST(N'2016-05-07' AS Date), CAST(N'2016-05-12' AS Date), 1, 2)
INSERT [dbo].[Rezervacije] ([RezervacijeID], [KnjigaID], [KorisnikID], [DatumPocetkaRezervacije], [DatumIstekaRezervacije], [Validna], [PrimerakID]) VALUES (10, 1, 4, CAST(N'2016-05-07' AS Date), CAST(N'2016-05-22' AS Date), 1, NULL)
SET IDENTITY_INSERT [dbo].[Rezervacije] OFF
SET IDENTITY_INSERT [dbo].[Signatura] ON 

INSERT [dbo].[Signatura] ([SignaturaID], [Naziv]) VALUES (1, N'Triler')
INSERT [dbo].[Signatura] ([SignaturaID], [Naziv]) VALUES (2, N'Klasika')
SET IDENTITY_INSERT [dbo].[Signatura] OFF
SET IDENTITY_INSERT [dbo].[VoteUp] ON 

INSERT [dbo].[VoteUp] ([VoteUpID], [KorisnikID], [KomentarID]) VALUES (22, 2, 2)
INSERT [dbo].[VoteUp] ([VoteUpID], [KorisnikID], [KomentarID]) VALUES (23, 2, 1)
SET IDENTITY_INSERT [dbo].[VoteUp] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_JMBG]    Script Date: 5/9/2016 8:40:34 AM ******/
ALTER TABLE [dbo].[Korisnik] ADD  CONSTRAINT [UQ_JMBG] UNIQUE NONCLUSTERED 
(
	[MaticniBroj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Primerak__39F21891A2E87339]    Script Date: 5/9/2016 8:40:34 AM ******/
ALTER TABLE [dbo].[Primerak] ADD UNIQUE NONCLUSTERED 
(
	[InventarskiBroj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Knjiga]  WITH CHECK ADD  CONSTRAINT [FK_KnjigaIzdavac] FOREIGN KEY([IzdavacID])
REFERENCES [dbo].[Izdavac] ([IzdavacID])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Knjiga] CHECK CONSTRAINT [FK_KnjigaIzdavac]
GO
ALTER TABLE [dbo].[Knjiga]  WITH CHECK ADD  CONSTRAINT [FK_Signatura] FOREIGN KEY([SignaturaID])
REFERENCES [dbo].[Signatura] ([SignaturaID])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Knjiga] CHECK CONSTRAINT [FK_Signatura]
GO
ALTER TABLE [dbo].[Komentar]  WITH CHECK ADD  CONSTRAINT [KnjigaKorisnikKom_FK] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Komentar] CHECK CONSTRAINT [KnjigaKorisnikKom_FK]
GO
ALTER TABLE [dbo].[Komentar]  WITH CHECK ADD  CONSTRAINT [KorisnikKnjigaKom_FK] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Komentar] CHECK CONSTRAINT [KorisnikKnjigaKom_FK]
GO
ALTER TABLE [dbo].[KorisnikGrupa]  WITH CHECK ADD  CONSTRAINT [FK_GrupaKorisnik] FOREIGN KEY([GrupaID])
REFERENCES [dbo].[Grupa] ([GrupaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[KorisnikGrupa] CHECK CONSTRAINT [FK_GrupaKorisnik]
GO
ALTER TABLE [dbo].[KorisnikGrupa]  WITH CHECK ADD  CONSTRAINT [FK_KorisnikGrupa] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[KorisnikGrupa] CHECK CONSTRAINT [FK_KorisnikGrupa]
GO
ALTER TABLE [dbo].[Na_Citanju]  WITH CHECK ADD  CONSTRAINT [FK_KnjigaNaCitanju] FOREIGN KEY([PrimerakID])
REFERENCES [dbo].[Primerak] ([PrimerakID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Na_Citanju] CHECK CONSTRAINT [FK_KnjigaNaCitanju]
GO
ALTER TABLE [dbo].[Na_Citanju]  WITH CHECK ADD  CONSTRAINT [FK_Korisnik] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Na_Citanju] CHECK CONSTRAINT [FK_Korisnik]
GO
ALTER TABLE [dbo].[Napisali]  WITH CHECK ADD  CONSTRAINT [FK_Autor_Knjiga] FOREIGN KEY([AutorID])
REFERENCES [dbo].[Autor] ([AutorID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Napisali] CHECK CONSTRAINT [FK_Autor_Knjiga]
GO
ALTER TABLE [dbo].[Napisali]  WITH CHECK ADD  CONSTRAINT [FK_KnjigaAutora] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Napisali] CHECK CONSTRAINT [FK_KnjigaAutora]
GO
ALTER TABLE [dbo].[ObavestenjeKorisnik]  WITH CHECK ADD  CONSTRAINT [KorisnikObavestenje_FK] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObavestenjeKorisnik] CHECK CONSTRAINT [KorisnikObavestenje_FK]
GO
ALTER TABLE [dbo].[ObavestenjeKorisnik]  WITH CHECK ADD  CONSTRAINT [ObavestenjeKorisnik_FK] FOREIGN KEY([ObavestenjeID])
REFERENCES [dbo].[Obavestenje] ([ObavestenjeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObavestenjeKorisnik] CHECK CONSTRAINT [ObavestenjeKorisnik_FK]
GO
ALTER TABLE [dbo].[Ocena]  WITH CHECK ADD  CONSTRAINT [FK_KorisnikOcena] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Ocena] CHECK CONSTRAINT [FK_KorisnikOcena]
GO
ALTER TABLE [dbo].[Ocena]  WITH CHECK ADD  CONSTRAINT [FK_OcenaKorisnik] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Ocena] CHECK CONSTRAINT [FK_OcenaKorisnik]
GO
ALTER TABLE [dbo].[Preuzeo]  WITH CHECK ADD  CONSTRAINT [KnjigaKorisnik_FK] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Preuzeo] CHECK CONSTRAINT [KnjigaKorisnik_FK]
GO
ALTER TABLE [dbo].[Preuzeo]  WITH CHECK ADD  CONSTRAINT [KorisnikKnjiga_FK] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Preuzeo] CHECK CONSTRAINT [KorisnikKnjiga_FK]
GO
ALTER TABLE [dbo].[PrijavaKomentara]  WITH CHECK ADD  CONSTRAINT [FK_PK_Komentar] FOREIGN KEY([KomentarId])
REFERENCES [dbo].[Komentar] ([KomentarID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PrijavaKomentara] CHECK CONSTRAINT [FK_PK_Komentar]
GO
ALTER TABLE [dbo].[PrijavaKomentara]  WITH CHECK ADD  CONSTRAINT [FK_PK_Korisnik] FOREIGN KEY([KorisnikId])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
GO
ALTER TABLE [dbo].[PrijavaKomentara] CHECK CONSTRAINT [FK_PK_Korisnik]
GO
ALTER TABLE [dbo].[Primerak]  WITH CHECK ADD  CONSTRAINT [FK_Knjiga] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Primerak] CHECK CONSTRAINT [FK_Knjiga]
GO
ALTER TABLE [dbo].[Primerak]  WITH CHECK ADD  CONSTRAINT [FK_VrstaPoveza] FOREIGN KEY([VrstaPovezaID])
REFERENCES [dbo].[Povezi] ([VrstaPovezaID])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Primerak] CHECK CONSTRAINT [FK_VrstaPoveza]
GO
ALTER TABLE [dbo].[Rezervacije]  WITH CHECK ADD  CONSTRAINT [FK_KnjigaKorisnikRez] FOREIGN KEY([KnjigaID])
REFERENCES [dbo].[Knjiga] ([KnjigaID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rezervacije] CHECK CONSTRAINT [FK_KnjigaKorisnikRez]
GO
ALTER TABLE [dbo].[Rezervacije]  WITH CHECK ADD  CONSTRAINT [FK_KorisnikKnjigaRez] FOREIGN KEY([KorisnikID])
REFERENCES [dbo].[Korisnik] ([KorisnikID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rezervacije] CHECK CONSTRAINT [FK_KorisnikKnjigaRez]
GO
/****** Object:  StoredProcedure [dbo].[usp_CitanjaPoDanu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CitanjaPoDanu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select DAY(r.DatumUzimanja)as 'Day', count(*) as 'count'
	from Na_Citanju r
	where YEAR(r.DatumUzimanja)=YEAR(@datum) and MONTH(r.DatumUzimanja)=MONTH(@datum)
	Group by DAY(r.DatumUzimanja)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_CitanjaPoMesecu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CitanjaPoMesecu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select Month(r.DatumUzimanja)as 'Day', count(*) as 'count'
	from Na_Citanju r
	where YEAR(r.DatumUzimanja)=YEAR(@datum)
	Group by Month(r.DatumUzimanja)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_PreuzimanjaPoDanu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PreuzimanjaPoDanu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select DAY(r.VremePreuzimanja)as 'Day', count(*) as 'count'
	from Preuzeo r
	where YEAR(r.VremePreuzimanja)=YEAR(@datum) and MONTH(r.VremePreuzimanja)=MONTH(@datum)
	Group by DAY(r.VremePreuzimanja)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_PreuzimanjaPoMesecu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PreuzimanjaPoMesecu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select Month(r.VremePreuzimanja)as 'Day', count(*) as 'count'
	from Preuzeo r
	where YEAR(r.VremePreuzimanja)=YEAR(@datum)
	Group by Month(r.VremePreuzimanja)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_RezervacijePoDanu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RezervacijePoDanu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select DAY(r.DatumPocetkaRezervacije)as 'Day', count(*) as 'count'
	from Rezervacije r
	where YEAR(r.DatumPocetkaRezervacije)=YEAR(@datum) and MONTH(r.DatumPocetkaRezervacije)=MONTH(@datum)
	Group by DAY(r.DatumPocetkaRezervacije)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_RezervacijePoMesecu]    Script Date: 5/9/2016 8:40:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RezervacijePoMesecu] 
	-- Add the parameters for the stored procedure here
	@datum as Datetime=null
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT off;

    -- Insert statements for procedure here
	select Month(r.DatumPocetkaRezervacije)as 'Day', count(*) as 'count'
	from Rezervacije r
	where YEAR(r.DatumPocetkaRezervacije)=YEAR(@datum)
	Group by Month(r.DatumPocetkaRezervacije)

END




GO
