﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using SkolskaBibliotekaDBContext;

namespace SkolskaBibliotekaService
{
    public partial class SkolskaBibliotekaService : ServiceBase
    {
        public SkolskaBibliotekaService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            this.WriteToFile("SkolskaBibliotekaService started {0}");
            this.ScheduleService();
        }

        protected override void OnStop()
        {
            this.WriteToFile("SkolskaBibliotekaService stopped {0}");
            this.Schedular.Dispose();
        }

        private Timer Schedular;

        public void ScheduleService()
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    Schedular = new Timer(new TimerCallback(SchedularCallback));
                    string mode = Podesavanja.Get("ModProvere");
                    this.WriteToFile("SkolskaBibliotekaService Mode: " + mode + " {0}");

                    //Set the Default Time.
                    DateTime scheduledTime = DateTime.MinValue;

                    if (mode == "DNEVNO")
                    {
                        //Get the Scheduled Time from AppSettings.
                        scheduledTime = DateTime.Parse(Podesavanja.Get("ScheduledTime"));
                        if (DateTime.Now > scheduledTime)
                        {
                            //If Scheduled Time is passed set Schedule for the next day.
                            scheduledTime = scheduledTime.AddDays(1);
                        }
                    }

                    if (mode.ToUpper() == "INTERVAL")
                    {
                        //Get the Interval in Minutes from AppSettings.
                        int intervalMinutes = Convert.ToInt32(Podesavanja.Get("IntervalMinutes"));

                        //Set the Scheduled Time by adding the Interval to Current Time.
                        scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                        if (DateTime.Now > scheduledTime)
                        {
                            //If Scheduled Time is passed set Schedule for the next Interval.
                            scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                        }
                    }

                    TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                    string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                    this.WriteToFile("SkolskaBibliotekaService scheduled to run after: " + schedule + " {0}");

                    //Get the difference in Minutes between the Scheduled and Current Time.
                    int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                    //Change the Timer's Due Time.
                    Schedular.Change(dueTime, Timeout.Infinite);
                }
            }
            catch (Exception ex)
            {
                WriteToFile("SkolskaBibliotekaService Error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedularCallback(object e)
        {
            this.WriteToFile("SkolskaBibliotekaService Log: {0}");
            DbUpdate();//poziv funkcije koja sredjuje bazu
            SendWarnings();
            this.ScheduleService();
        }

        private void WriteToFile(string text)
        {
            string path = "C:\\ServiceLog.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }

        public bool DbUpdate()
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    List<Rezervacije> lista = db.Rezervacije.Where(x => x.DatumIstekaRezervacije < DateTime.Now && x.Validna == true).ToList();
                    foreach (Rezervacije r in lista)
                    {
                        r.Validna = false;
                        Primerak p = db.Primerak.FirstOrDefault(x => x.PrimerakID == r.PrimerakID);
                        if (p != null)
                        {
                            p.Aktivna = true;
                        }
                        db.SaveChanges();
                        //TODO: Dodati log 
                    }

                    lista.Clear();

                    

                    lista = db.Rezervacije.Where(x => x.DatumIstekaRezervacije >= DateTime.Now && x.Validna == true && x.PrimerakID == null).ToList();
                    foreach (Rezervacije r in lista)
                    {
                        Primerak p = db.Primerak.FirstOrDefault(x => x.KnjigaID == r.KnjigaID && x.Aktivna == true);
                        if (p != null)
                        {
                            var o = db.Obavestenje.Create();
                            o.Kreator = Podesavanja.Get("KreatorSistemskePoruke");
                            o.PostDate = DateTime.Now;
                            o.Tekst = String.Format(Podesavanja.Get("TekstRezervacijePoruke"),p.Knjiga.Naziv, Podesavanja.Get("DuzinaRezervacije"));
                            db.Obavestenje.Add(o);
                            db.SaveChanges();

                            r.PrimerakID = p.PrimerakID;
                            p.Aktivna = false;
                            r.DatumIstekaRezervacije = DateTime.Now.AddDays(Convert.ToDouble(db.Settings.FirstOrDefault(x => x.Name == "DuzinaRezervacije").Value));

                            var obj = db.ObavestenjeKorisnik.Create();
                            obj.Obavestenje = o;

                            r.Korisnik.ObavestenjeKorisnik.Add(obj);
                            db.SaveChanges();

                            WriteToFile("SkolskaBibliotekaService Log on: {0} "+o.Tekst+" za korisnika "+r.Korisnik.Username);
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteToFile(ex.GetBaseException().Message);
                return false;
            }
        }

        public bool SendWarnings()
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    List<Na_Citanju> lista = db.Na_Citanju.Where(x => x.RokZaVracanje < DateTime.Now && x.DatumVracanja == null).ToList();

                   

                    foreach (Na_Citanju n in lista)
                    {
                        var o = db.Obavestenje.Create();
                        o.Kreator = Podesavanja.Get("KreatorSistemskePoruke");
                        o.PostDate = DateTime.Now;
                        o.Tekst = string.Format(Podesavanja.Get("TekstSistemskePoruke"), n.Primerak.Knjiga.Naziv, n.DatumUzimanja, n.RokZaVracanje);
                        db.Obavestenje.Add(o);
                        db.SaveChanges();

                        var obj = db.ObavestenjeKorisnik.Create();
                        obj.Obavestenje = o;

                        n.Korisnik.ObavestenjeKorisnik.Add(obj);
                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteToFile("SkolskaBibliotekaService Error on: {0} " + ex.GetBaseException().Message + ex.StackTrace);
                return false;
            }
        }
    }
}
