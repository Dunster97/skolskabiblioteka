﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Autor")]
    public partial class Autor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Autor()
        {
            Knjiga = new HashSet<Knjiga>();
        }

        [Display(Name = "Аутор")]
        public int AutorID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Име")]
        public string Ime { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Презиме")]
        public string Prezime { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Датум рођења")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DatumRodjenja { get; set; }

        [StringLength(100)]
        [Display(Name = "Адреса")]
        public string Adresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Knjiga> Knjiga { get; set; }
    }
}
