﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Grupa")]
    public partial class Grupa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Grupa()
        {
            KorisnikGrupa = new HashSet<KorisnikGrupa>();
        }

        [Display(Name = "Група")]
        public int GrupaID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Име")]
        public string Naziv { get; set; }

        [Display(Name = "Ниво приступа")]
        public int? AccessLevel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KorisnikGrupa> KorisnikGrupa { get; set; }
    }
}
