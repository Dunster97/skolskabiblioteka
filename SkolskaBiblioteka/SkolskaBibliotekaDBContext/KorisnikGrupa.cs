﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KorisnikGrupa")]
    public partial class KorisnikGrupa
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Група")]
        public int GrupaID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Датум додавања")]
        public DateTime DatumDodavanja { get; set; }

        public virtual Grupa Grupa { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
