﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ocena")]
    public partial class Ocena
    {
        [Display(Name = "Оцена")]
        public int OcenaID { get; set; }

        public int? KorisnikID { get; set; }

        public int? KnjigaID { get; set; }

        [Column("Ocena")]
        public int? Ocena1 { get; set; }

        public virtual Knjiga Knjiga { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
