﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Signatura")]
    public partial class Signatura
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Signatura()
        {
            Knjiga = new HashSet<Knjiga>();
        }

        [Display(Name = "Сигнатура")]
        public int SignaturaID { get; set; }

        [StringLength(50)]
        [Display(Name = "Назив")]
        public string Naziv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Knjiga> Knjiga { get; set; }
    }
}
