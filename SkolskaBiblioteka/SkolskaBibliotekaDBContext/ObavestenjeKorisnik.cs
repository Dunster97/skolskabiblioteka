﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ObavestenjeKorisnik")]
    public partial class ObavestenjeKorisnik
    {
        [Display(Name = "Идентификација")]
        public int ObavestenjeKorisnikID { get; set; }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Display(Name = "Обавештење")]
        public int ObavestenjeID { get; set; }

        [Display(Name = "Датум виђења")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm:ss}")]
        public DateTime? SeenDate { get; set; }

        [Display(Name = "Виђено")]
        public bool? Seen { get; set; }

        [Column(TypeName = "DateTime2")]
        [Display(Name = "Датум слања")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm:ss}")]
        public DateTime? DatumSlanja { get; set; }

        public virtual Korisnik Korisnik { get; set; }

        public virtual Obavestenje Obavestenje { get; set; }
    }
}
