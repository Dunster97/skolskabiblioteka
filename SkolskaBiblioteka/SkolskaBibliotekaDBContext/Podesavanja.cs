﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolskaBibliotekaDBContext
{
    public class Podesavanja
    {
        public static string Get(string key)
        {
            using(SkolskaBibliotekaDBModel db=new SkolskaBibliotekaDBModel())
            {
                return db.Settings.FirstOrDefault(x => x.Name == key).Value;
            }
        }
    }
}
