﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PrijavaKomentara")]
    public partial class PrijavaKomentara
    {
        [Display(Name = "Идентификација")]
        public int PrijavaKomentaraId { get; set; }

        [Display(Name = "Коментар")]
        public int? KomentarId { get; set; }

        [Display(Name = "Корисник")]
        public int? KorisnikId { get; set; }

        [Required(ErrorMessage = "Морате образложити своју пријаву.")]
        [MinLength(30,ErrorMessage = "Морате унети минимално 30 карактера.")]
        [Display(Name = "Разлог")]
        public string Tekst { get; set; }

        [Display(Name = "Време пријаве")]
        public DateTime? VremePrijave { get; set; }

        public virtual Komentar Komentar { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
