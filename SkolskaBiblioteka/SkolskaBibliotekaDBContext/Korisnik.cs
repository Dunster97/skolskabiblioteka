﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("Korisnik")]
    public partial class Korisnik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Korisnik()
        {
            Komentar = new HashSet<Komentar>();
            Na_Citanju = new HashSet<Na_Citanju>();
            KorisnikGrupa = new HashSet<KorisnikGrupa>();
            Rezervacije = new HashSet<Rezervacije>();
            Ocena = new HashSet<Ocena>();
            PrijavaKomentara = new HashSet<PrijavaKomentara>();
            Preuzeo = new HashSet<Preuzeo>();
            ObavestenjeKorisnik = new HashSet<ObavestenjeKorisnik>();
        }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Required(ErrorMessage = "Морате унети матични број(ЈМБГ).")]
        [MinLength(13, ErrorMessage = "Матични број(ЈМБГ) мора имати 13 цифара.")]
        [MaxLength(13, ErrorMessage = "Матични број(ЈМБГ) мора имати 13 цифара.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Матични број(ЈМБГ) мора садржати само цифре.")]
        [StringLength(15)]
        [Display(Name = "Матични број(ЈМБГ)")]
        [Remote("jmbgIsUnique", "Account", HttpMethod = "POST", ErrorMessage = "ЈМБГ већ постоји!")]
        public string MaticniBroj { get; set; }

        [Required(ErrorMessage = "Морате унети име.")]
        [StringLength(50)]
        [Display(Name = "Име")]
        public string Ime { get; set; }

        [Required(ErrorMessage = "Морате унети презиме.")]
        [StringLength(50)]
        [Display(Name = "Презиме")]
        public string Prezime { get; set; }

        [StringLength(50)]
        [Display(Name = "Место")]
        public string Mesto { get; set; }

        [StringLength(100)]
        [Display(Name = "Адреса")]
        public string Adresa { get; set; }

        [StringLength(20)]
        [Display(Name = "Број телефона")]
        public string Telefon { get; set; }

        [StringLength(10)]
        public string ClanskaKartaBroj { get; set; }

        [StringLength(50)]
        [Display(Name = "Бар код")]
        public string BarCode { get; set; }

        [Required(ErrorMessage = "Морате унети емаил.")]
        [StringLength(50)]
        [Display(Name = "Емаил")]
        public string Email { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Морате унети корисничко име.")]
        [Display(Name = "Кориснично име")]
        [Remote("usernameIsUnique", "Account", HttpMethod = "POST", ErrorMessage = "Корисничко име већ постоји!")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Лозинка мора имати више од 5 цифара.")]
        [Required(ErrorMessage = "Морате унети лозинку.")]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }

        public string PasswordSalt { get; set; }

        public string PicPath { get; set; }

        public int? LoginAttempt { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LockedDateTime { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Лозинке се морају поклапати.")]
        [DataType(DataType.Password)]
        [Display(Name = "Потврда лозинке")]
        public string ConfirmPassword { get; set; }

        public DateTime? LatestVisit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Komentar> Komentar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Na_Citanju> Na_Citanju { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KorisnikGrupa> KorisnikGrupa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezervacije> Rezervacije { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ocena> Ocena { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrijavaKomentara> PrijavaKomentara { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Preuzeo> Preuzeo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ObavestenjeKorisnik> ObavestenjeKorisnik { get; set; }
    }
}
