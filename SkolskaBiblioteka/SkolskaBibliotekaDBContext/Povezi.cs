﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Povezi")]
    public partial class Povezi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Povezi()
        {
            Primerak = new HashSet<Primerak>();
        }

        [Key]
        [Display(Name = "Повез")]
        public int VrstaPovezaID { get; set; }

        [StringLength(50)]
        [Display(Name = "Назив")]
        public string Naziv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Primerak> Primerak { get; set; }
    }
}
