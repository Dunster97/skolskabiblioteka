﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Obavestenje")]
    public partial class Obavestenje
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Obavestenje()
        {
            ObavestenjeKorisnik = new HashSet<ObavestenjeKorisnik>();
        }

        [Display(Name = "Обавештење")]
        public int ObavestenjeID { get; set; }

        [StringLength(100)]
        [Display(Name = "Креатор")]
        public string Kreator { get; set; }

        [Required(ErrorMessage ="Морате унети текст поруке.")]
        [MinLength(20,ErrorMessage ="Морате унети минимално 20 карактера.")]
        [Display(Name = "Текст")]
        public string Tekst { get; set; }

        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm:ss}")]
        [Display(Name = "Датум")]
        public DateTime PostDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ObavestenjeKorisnik> ObavestenjeKorisnik { get; set; }
    }
}
