﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Komentar")]
    public partial class Komentar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Komentar()
        {
            PrijavaKomentara = new HashSet<PrijavaKomentara>();
        }

        [Display(Name = "Коментар")]
        public int KomentarID { get; set; }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Display(Name = "Књига")]
        public int KnjigaID { get; set; }

        [Display(Name = "Време")]
        public DateTime VremeKomentara { get; set; }

        [Display(Name = "Пријављен")]
        public bool Reported { get; set; }

        [MinLength(10,ErrorMessage ="Порука мора садржати минимално 10 карактера.")]
        [MaxLength(500, ErrorMessage = "Порука мора садржати максимално 500 карактера.")]
        [Required(ErrorMessage = "Порука мора садржати минимално 10 карактера.")]
        [Display(Name = "Текст")]
        public string KomentarText { get; set; }

        [Display(Name = "+")]
        public int? VoteUp { get; set; }

        [Display(Name = "-")]
        public int? VoteDown { get; set; }

        public virtual Knjiga Knjiga { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrijavaKomentara> PrijavaKomentara { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
