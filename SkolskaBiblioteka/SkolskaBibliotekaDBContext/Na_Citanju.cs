﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Na_Citanju
    {
        [Display(Name = "УДК")]
        public int Na_CitanjuID { get; set; }

        [Display(Name = "Примерак")]
        public int PrimerakID { get; set; }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Датум узимања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumUzimanja { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Датум враћања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DatumVracanja { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Рок за враћање")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? RokZaVracanje { get; set; }

        public virtual Korisnik Korisnik { get; set; }

        public virtual Primerak Primerak { get; set; }
    }
}
