namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VoteDown")]
    public partial class VoteDown
    {
        public int VoteDownID { get; set; }

        public int? KorisnikID { get; set; }

        public int? KomentarID { get; set; }
    }
}
