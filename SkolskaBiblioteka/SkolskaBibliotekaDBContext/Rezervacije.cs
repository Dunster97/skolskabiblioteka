﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rezervacije")]
    public partial class Rezervacije
    {
        [Display(Name = "Резервација")]
        public int RezervacijeID { get; set; }

        [Display(Name = "Књига")]
        public int KnjigaID { get; set; }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Почетак резервације")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumPocetkaRezervacije { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Крај резервације")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumIstekaRezervacije { get; set; }

        [Display(Name = "Валидна")]
        public bool Validna { get; set; }

        [Display(Name = "Примерак")]
        public int? PrimerakID { get; set; }

        public virtual Knjiga Knjiga { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
