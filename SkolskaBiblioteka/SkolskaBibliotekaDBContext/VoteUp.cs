namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VoteUp")]
    public partial class VoteUp
    {
        public int VoteUpID { get; set; }

        public int? KorisnikID { get; set; }

        public int? KomentarID { get; set; }
    }
}
