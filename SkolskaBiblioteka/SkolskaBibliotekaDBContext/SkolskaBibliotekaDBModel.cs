namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SkolskaBibliotekaDBModel : DbContext
    {
        public SkolskaBibliotekaDBModel()
            : base("name=SkolskaBibliotekaDBModel")
        {
        }

        public virtual DbSet<Autor> Autor { get; set; }
        public virtual DbSet<Grupa> Grupa { get; set; }
        public virtual DbSet<Izdavac> Izdavac { get; set; }
        public virtual DbSet<Knjiga> Knjiga { get; set; }
        public virtual DbSet<Komentar> Komentar { get; set; }
        public virtual DbSet<Korisnik> Korisnik { get; set; }
        public virtual DbSet<KorisnikGrupa> KorisnikGrupa { get; set; }
        public virtual DbSet<Na_Citanju> Na_Citanju { get; set; }
        public virtual DbSet<Obavestenje> Obavestenje { get; set; }
        public virtual DbSet<ObavestenjeKorisnik> ObavestenjeKorisnik { get; set; }
        public virtual DbSet<Ocena> Ocena { get; set; }
        public virtual DbSet<Povezi> Povezi { get; set; }
        public virtual DbSet<Preuzeo> Preuzeo { get; set; }
        public virtual DbSet<PrijavaKomentara> PrijavaKomentara { get; set; }
        public virtual DbSet<Primerak> Primerak { get; set; }
        public virtual DbSet<Rezervacije> Rezervacije { get; set; }
        public virtual DbSet<Signatura> Signatura { get; set; }
        public virtual DbSet<VoteDown> VoteDown { get; set; }
        public virtual DbSet<VoteUp> VoteUp { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autor>()
                .HasMany(e => e.Knjiga)
                .WithMany(e => e.Autor)
                .Map(m => m.ToTable("Napisali").MapLeftKey("AutorID").MapRightKey("KnjigaID"));

            modelBuilder.Entity<Knjiga>()
                .HasMany(e => e.Ocena)
                .WithOptional(e => e.Knjiga)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Korisnik>()
                .Property(e => e.Telefon)
                .IsUnicode(false);

            modelBuilder.Entity<Korisnik>()
                .HasMany(e => e.Ocena)
                .WithOptional(e => e.Korisnik)
                .WillCascadeOnDelete();
        }
    }
}
