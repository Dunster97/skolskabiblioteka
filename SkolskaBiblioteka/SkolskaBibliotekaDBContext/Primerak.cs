﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Primerak")]
    public partial class Primerak
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Primerak()
        {
            Na_Citanju = new HashSet<Na_Citanju>();
        }

        [Display(Name = "Примерак")]
        public int PrimerakID { get; set; }

        [Display(Name = "Књига")]
        public int KnjigaID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Инвентарски")]
        public string InventarskiBroj { get; set; }

        [Display(Name = "Активна")]
        public bool Aktivna { get; set; }

        [Display(Name = "Депо")]
        public bool? Depo { get; set; }

        [Display(Name = "Врста повеза")]
        public int? VrstaPovezaID { get; set; }

        [StringLength(50)]
        [Display(Name = "Бар код")]
        public string BarCode { get; set; }

        public virtual Knjiga Knjiga { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Na_Citanju> Na_Citanju { get; set; }

        public virtual Povezi Povezi { get; set; }
    }
}
