﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Preuzeo")]
    public partial class Preuzeo
    {
        [Display(Name = "Преузео")]
        public int PreuzeoID { get; set; }

        [Display(Name = "Корисник")]
        public int KorisnikID { get; set; }

        [Display(Name = "Књига")]
        public int KnjigaID { get; set; }

        [Display(Name = "Датум и време преузимања")]
        public DateTime VremePreuzimanja { get; set; }

        public virtual Knjiga Knjiga { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
