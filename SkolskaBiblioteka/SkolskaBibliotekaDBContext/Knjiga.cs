﻿namespace SkolskaBibliotekaDBContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Knjiga")]
    public partial class Knjiga
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Knjiga()
        {
            Primerak = new HashSet<Primerak>();
            Rezervacije = new HashSet<Rezervacije>();
            Ocena = new HashSet<Ocena>();
            Preuzeo = new HashSet<Preuzeo>();
            Komentar = new HashSet<Komentar>();
            Autor = new HashSet<Autor>();
        }

        public int KnjigaID { get; set; }

        [StringLength(50)]
        [Display(Name = "УДК")]
        public string UDK { get; set; }

        [StringLength(50)]
        [Display(Name = "ИСБН")]
        public string ISBN { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Наслов")]
        public string Naziv { get; set; }

        [Display(Name = "Сигнатура")]
        public int? SignaturaID { get; set; }

        [Display(Name = "Слика")]
        public string PicPath { get; set; }

        [Display(Name = "Издавач")]
        public int? IzdavacID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Година издавања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DatumIzdavanja { get; set; }

        [Display(Name = "Преузимање")]
        public string PDFPath { get; set; }

        [Display(Name = "Папирни примерци")]
        public bool? PapirnoIzdanje { get; set; }

        [Display(Name = "Опис")]
        public string Opis { get; set; }

        public virtual Izdavac Izdavac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Primerak> Primerak { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezervacije> Rezervacije { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ocena> Ocena { get; set; }

        public virtual Signatura Signatura { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Preuzeo> Preuzeo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Komentar> Komentar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Autor> Autor { get; set; }
    }
}
