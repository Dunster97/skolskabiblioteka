﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServiceInstaller
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Check();
            timer1.Interval =1000;
            timer1.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServiceInstaller.InstService(@"C:\Users\stefan.petkovic\Desktop\skolskabiblioteka\SkolskaBiblioteka\SkolskaBibliotekaService\bin\Debug\SkolskaBibliotekaService.exe", "SkolskaBibliotekaService");

            //Form1_Load(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ServiceInstaller.UnInstallService("SkolskaBibliotekaService");

            GC.Collect();

            //Form1_Load(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ServiceInstaller.IsServiceInstalled("SkolskaBibliotekaService"))
            {
                ServiceInstaller.StartService("SkolskaBibliotekaService", -1);
            }  
            //Form1_Load(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (ServiceInstaller.IsServiceInstalled("SkolskaBibliotekaService"))
            {
                ServiceInstaller.RestartService("SkolskaBibliotekaService", -1);
            }
            //Form1_Load(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ServiceInstaller.IsServiceInstalled("SkolskaBibliotekaService"))
            {
                ServiceInstaller.StopService("SkolskaBibliotekaService", -1);
            }
            //Form1_Load(sender, e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Check();
        }

        private void Check()
        {
            if (ServiceInstaller.IsServiceInstalled("SkolskaBibliotekaService"))
            {
                label1.Text = "Servis je instaliran.";
                label2.Text = "";
                var x = ServiceInstaller.GetService("SkolskaBibliotekaService");


                if (x.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                {
                    label2.Text = "Servis je pokrenut.";
                }
                else
                if (x.Status == System.ServiceProcess.ServiceControllerStatus.Paused)
                {
                    label2.Text = "Servis je pauziran.";
                }
                else
                {
                    label2.Text = "Servis je zaustavljen.";
                }
            }
            else
            {
                label1.Text = "Servis nije instaliran.";
                label2.Text = "";
            }
        }
    }
}
