﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Runtime.InteropServices;

namespace ServiceInstaller
{
    public class ServiceInstaller
    {
        #region "Private Variables"
        private string _servicePath;
        private string _serviceName;
        #endregion

        private string _serviceDisplayName;

        #region "DLLImport"
        [DllImport("advapi32.dll")]
        public static extern IntPtr OpenSCManager(string lpMachineName, string lpSCDB, int scParameter);
        [DllImport("Advapi32.dll")]
        public static extern IntPtr CreateService(IntPtr SC_HANDLE, string lpSvcName, string lpDisplayName, int dwDesiredAccess, int dwServiceType, int dwStartType, int dwErrorControl, string lpPathName, string lpLoadOrderGroup, int lpdwTagId,
        string lpDependencies, string lpServiceStartName, string lpPassword);
        [DllImport("advapi32.dll")]
        public static extern void CloseServiceHandle(IntPtr SCHANDLE);
        [DllImport("advapi32.dll")]
        public static extern int StartService(IntPtr SVHANDLE, int dwNumServiceArgs, string lpServiceArgVectors);
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern IntPtr OpenService(IntPtr SCHANDLE, string lpSvcName, int dwNumServiceArgs);
        [DllImport("advapi32.dll")]
        public static extern int DeleteService(IntPtr SVHANDLE);
        [DllImport("kernel32.dll")]
        public static extern int GetLastError();
        #endregion
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static void InstService(string vSvcPath, string vServiceName)
        {
            //#Region "Testing"
            // Testing --------------
            string svcPath = null;
            string svcName = null;
            string svcDispName = null;
            //path to the service that you want to install
            svcPath = vSvcPath;
            svcDispName = vServiceName;
            svcName = vServiceName;
            ServiceInstaller c = new ServiceInstaller();
            c.InstallService(svcPath, svcName, svcDispName);
            //  Testing--------------;
            //#End Region
            //Console.Read()
        }


        /// <summary>
        /// This method installs and runs the service in the service control manager.
        /// </summary>
        /// <param name="svcPath">The complete path of the service.</param>
        /// <param name="svcName">Name of the service.</param>
        /// <param name="svcDispName">Display name of the service.</param>
        /// <returns>True if the process went thro successfully. False if there was any error.</returns>
        public bool InstallService(string svcPath, string svcName, string svcDispName)
        {
            //#Region "Constants declaration."
            int SC_MANAGER_CREATE_SERVICE = 0x2;
            int SERVICE_WIN32_OWN_PROCESS = 0x10;
            //int SERVICE_DEMAND_START = 0x00000003;
            int SERVICE_ERROR_NORMAL = 0x1;
            int STANDARD_RIGHTS_REQUIRED = 0xf0000;
            int SERVICE_QUERY_CONFIG = 0x1;
            int SERVICE_CHANGE_CONFIG = 0x2;
            int SERVICE_QUERY_STATUS = 0x4;
            int SERVICE_ENUMERATE_DEPENDENTS = 0x8;
            int SERVICE_START = 0x10;
            int SERVICE_STOP = 0x20;
            int SERVICE_PAUSE_CONTINUE = 0x40;
            int SERVICE_INTERROGATE = 0x80;
            int SERVICE_USER_DEFINED_CONTROL = 0x100;
            int SERVICE_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | SERVICE_QUERY_CONFIG | SERVICE_CHANGE_CONFIG | SERVICE_QUERY_STATUS | SERVICE_ENUMERATE_DEPENDENTS | SERVICE_START | SERVICE_STOP | SERVICE_PAUSE_CONTINUE | SERVICE_INTERROGATE | SERVICE_USER_DEFINED_CONTROL);
            int SERVICE_AUTO_START = 0x2;
            //#End Region
            try
            {
                IntPtr sc_handle = OpenSCManager(null, null, SC_MANAGER_CREATE_SERVICE);
                if (sc_handle != IntPtr.Zero)
                {
                    IntPtr sv_handle = CreateService(sc_handle, svcName, svcDispName, SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, svcPath, null, 0,
                    null, null, null);
                    if (sv_handle != IntPtr.Zero)
                    {
                        CloseServiceHandle(sc_handle);
                        return false;
                    }
                    else
                    {
                        //now trying to start the service
                        int i = StartService(sv_handle, 0, null);
                        // If the value i is zero, then there was an error starting the service.
                        // note: error may arise if the service is already running or some other problem.
                        if (i == 0)
                        {
                            //Console.WriteLine("Couldnt start service");
                            return false;
                        }
                        //Console.WriteLine("Success");
                        CloseServiceHandle(sc_handle);
                        return true;
                    }
                }
                else
                {
                    //Console.WriteLine("SCM not opened successfully");
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This method uninstalls the service from the service conrol manager.
        /// </summary>
        /// <param name="svcName">Name of the service to uninstall.</param>
        public static bool UnInstallService(string svcName)
        {
            int GENERIC_WRITE = 0x40000000;
            IntPtr sc_hndl = OpenSCManager(null, null, GENERIC_WRITE);
            if (sc_hndl != IntPtr.Zero)
            {
                int DELETE = 0x10000;
                IntPtr svc_hndl = OpenService(sc_hndl, svcName, DELETE);
                //Console.WriteLine(svc_hndl.ToInt32());
                if (svc_hndl != IntPtr.Zero)
                {
                    int i = DeleteService(svc_hndl);
                    if (i != 0)
                    {
                        CloseServiceHandle(sc_hndl);
                        return true;
                    }
                    else
                    {
                        CloseServiceHandle(sc_hndl);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsServiceInstalled(string serviceName)
        {
            // get list of Windows services
            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController service in services)
            {
                if (service.ServiceName == serviceName)
                {
                    return true;
                }
            }
            return false;
        }

        public static ServiceController GetService(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            return Array.Find(services, (ServiceController service) => service.ServiceName == serviceName);
        }

        public static System.ServiceProcess.ServiceControllerStatus StartService(string serviceName, int timeoutMilliseconds)
        {
            // Dim service As New ServiceController(serviceName)
            ServiceController service = GetService(serviceName);

            if (service == null)
                return ServiceControllerStatus.StopPending;
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);

                return ServiceControllerStatus.StartPending;
            }
            catch
            {
                return ServiceControllerStatus.StopPending;
            }
        }

        public static System.ServiceProcess.ServiceControllerStatus StopService(string serviceName, int timeoutMilliseconds)
        {
            // Dim service As New ServiceController(serviceName)
            ServiceController service = GetService(serviceName);
            if (service == null)
                return ServiceControllerStatus.StopPending;
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                return service.Status;
            }
            catch
            {
                return ServiceControllerStatus.StopPending;
                // ...
            }

        }

        public static System.ServiceProcess.ServiceControllerStatus RestartService(string serviceName, int timeoutMilliseconds)
        {
            // Dim service As New ServiceController(serviceName)
            ServiceController service = GetService(serviceName);
            if (service == null)
                return ServiceControllerStatus.StopPending;
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                return ServiceControllerStatus.StartPending;
            }
            catch
            {
                return ServiceControllerStatus.StopPending;
            }
        }
    }
}
