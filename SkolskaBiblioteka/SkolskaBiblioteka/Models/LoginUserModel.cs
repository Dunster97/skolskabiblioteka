﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.Models
{
    public class LoginUserModel
    {
        [StringLength(50)]
        [Required(ErrorMessage = "Морате унети корисничко име.")]
        [Display(Name = "Кориснично име")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Морате унети лозинку.")]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }
    }
}