﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SkolskaBiblioteka.ViewModels;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.Models
{
    public class LoadAuth : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
                if (filterContext.RequestContext.HttpContext.Session["Korisnik"] == null)
                {
                    using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                        filterContext.RequestContext.HttpContext.Session["Korisnik"] = new KorisnikZaPrikaz(db.Korisnik.FirstOrDefault(x => x.Username == filterContext.RequestContext.HttpContext.User.Identity.Name));
                }

            this.OnActionExecuting(filterContext);
        }
    }
}