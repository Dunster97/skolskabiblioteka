﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.ViewModels
{
    public class IstorijaZaPrikaz
    {
        public List<PreuzeoZaPrikaz> PreuzeteKnjige {get;set;}
        public List<NaCitanjuZaPrikaz> NaCitanju { get; set; }
        public List<RezervacijeZaPrikaz> Rezervisane { get; set; }
        public List<KomentarZaPrikaz> Komentari { get; set; }

        public IstorijaZaPrikaz()
        {
            PreuzeteKnjige = new List<PreuzeoZaPrikaz>();
            NaCitanju = new List<NaCitanjuZaPrikaz>();
            Rezervisane = new List<RezervacijeZaPrikaz>();
            Komentari = new List<KomentarZaPrikaz>();
        }
    }
}