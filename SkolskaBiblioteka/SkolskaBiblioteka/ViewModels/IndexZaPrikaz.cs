﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.ViewModels
{
    public class IndexZaPrikaz
    {

        public List<KomentarZaPrikaz> Komentari;
        public List<KnjigaZaPrikaz> Knjige;

        public IndexZaPrikaz()
        {
            Komentari = new List<KomentarZaPrikaz>();
            Knjige = new List<KnjigaZaPrikaz>();
        }

    }
}