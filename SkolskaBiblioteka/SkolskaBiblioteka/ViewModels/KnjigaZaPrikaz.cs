﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.ViewModels
{
    public class KnjigaZaPrikaz
    {
        public int KnjigaID { get; set; }

        [Display(Name = "УДК")]
        [StringLength(50)]
        public string UDK { get; set; }

        [Display(Name = "ИСБН")]
        [StringLength(50)]
        public string ISBN { get; set; }

        [Required]
        [Display(Name = "Назив књиге")]
        [StringLength(100)]
        public string Naziv { get; set; }

        [Display(Name = "Слика")]
        public string PicPath { get; set; }

        [Display(Name = "Година издавања")]
        public DateTime? DatumIzdavanja { get; set; }

        [Display(Name = "Линк за преузимање")]
        public string PDFPath { get; set; }

        [Display(Name = "Да ли постоји у папирној верзији")]
        public bool? PapirnoIzdanje { get; set; }

        [Display(Name = "Назив издавача")]
        public string Izdavac { get; set; }

        [Display(Name = "Назив сигнатуре/жанра")]
        public string Signatura { get; set; }

        [Display(Name ="Имена аутора")]
        public List<string> Autori { get; set; }

        public int Ocena { get; set; }

        public KnjigaZaPrikaz(Knjiga k)
        {
            KnjigaID = k.KnjigaID;
            Naziv = k.Naziv;
            PicPath = k.PicPath;
            if(k.DatumIzdavanja!=null)
            DatumIzdavanja = k.DatumIzdavanja;
            PDFPath = k.PDFPath;
            PapirnoIzdanje = k.PapirnoIzdanje;
            if(k.Izdavac!=null)
            Izdavac = k.Izdavac.Naziv;
            if (k.Signatura != null)
                Signatura = k.Signatura.Naziv;

            Autori = new List<string>();

            foreach (Autor a in k.Autor)
            {
                Autori.Add(a.Ime + " " + a.Prezime);
            }

            double? o=k.Ocena.Average(x => x.Ocena1);

            if (o != null)
            {
                Ocena = (int)Math.Round((double)o);
            }
            else
                Ocena = 0;
        }
    }
}