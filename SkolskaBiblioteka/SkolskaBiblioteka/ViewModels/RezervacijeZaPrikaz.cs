﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;
using System.ComponentModel.DataAnnotations;

namespace SkolskaBiblioteka.ViewModels
{
    public class RezervacijeZaPrikaz
    {
        [Display(Name = "Наслов")]
        public string Naziv { get; set; }

        [Display(Name = "Датум почетка резервације")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumPocetkaRezervacije { get; set; }

        [Display(Name = "Датум истека резервације")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumIstekaRezervacije { get; set; }

        [Display(Name = "Валидна")]
        public bool Validna { get; set; }

        public RezervacijeZaPrikaz(Rezervacije r)
        {
            Naziv = r.Knjiga.Naziv;
            DatumIstekaRezervacije = r.DatumIstekaRezervacije;
            DatumPocetkaRezervacije = r.DatumPocetkaRezervacije;
            Validna = r.Validna;
        }
    }
}