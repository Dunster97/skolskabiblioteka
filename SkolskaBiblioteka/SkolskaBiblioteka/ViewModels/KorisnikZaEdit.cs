﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SkolskaBiblioteka.ViewModels
{
    public class KorisnikZaEdit
    {

        [Display(Name = "Име")]
        public string Ime { get; set; }

        [Display(Name = "Презиме")]
        public string Prezime { get; set; }

        [Display(Name = "Место")]
        public string Mesto { get; set; }

        [Display(Name = "Адреса")]
        public string Adresa { get; set; }

        [Display(Name = "Број телефона")]
        public string Telefon { get; set; }

        [Required(ErrorMessage = "Морате унети емаил.")]
        [Display(Name = "Емаил(*)")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail адреса није валидна")]
        public string Email { get; set; }

        [Display(Name = "Слика")]
        public string PicPath { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Тренутна лозинка(*)")]
        [Required(ErrorMessage = "Морате унети тренутну лозинку.")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Нова лозинка")]
        [MinLength(6, ErrorMessage = "Лозинка мора имати више од 5 карактера.")]
        [MaxLength(35, ErrorMessage = "Лозинка мора имати мање од 36 карактера.")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Лозинке се морају поклапати.")]
        [Display(Name = "Потврда нове лозинке")]
        public string ConfirmNewPassword { get; set; }

        public KorisnikZaEdit(Korisnik k)
        {
            Ime = k.Ime;
            Prezime = k.Prezime;
            Mesto = k.Mesto;
            Adresa = k.Adresa;
            Telefon = k.Telefon;
            Email = k.Email;
            PicPath = k.PicPath;
        }

        public KorisnikZaEdit()
        {
        }
    }
}