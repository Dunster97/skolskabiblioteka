﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;
using System.ComponentModel.DataAnnotations;

namespace SkolskaBiblioteka.ViewModels
{
    public class NaCitanjuZaPrikaz
    {
        [Display(Name = "Наслов")]
        public string Naziv { get; set; }

        [Display(Name = "Датум узимања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DatumUzimanja { get; set; }

        [Display(Name = "Датум враћања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DatumVracanja { get; set; }

        [Display(Name = "Рок за враћање")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? RokZaVracanje { get; set; }

        [Display(Name = "Инвентарски број")]
        public string InventarskiBroj { get; set; }

        public NaCitanjuZaPrikaz(Na_Citanju nc)
        {
            Naziv = nc.Primerak.Knjiga.Naziv;
            DatumUzimanja = nc.DatumUzimanja;
            DatumVracanja = nc.DatumVracanja;
            RokZaVracanje = nc.RokZaVracanje;
            InventarskiBroj = nc.Primerak.InventarskiBroj;
        }
    }

}