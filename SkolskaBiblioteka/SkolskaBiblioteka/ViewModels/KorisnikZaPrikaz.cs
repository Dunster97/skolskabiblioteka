﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;
using System.ComponentModel.DataAnnotations;

namespace SkolskaBiblioteka.ViewModels
{
    public class KorisnikZaPrikaz
    {
        [Display(Name = "Име")]
        public string Ime { get; set; }

        [Display(Name = "Презиме")]
        public string Prezime { get; set; }

        [Display(Name = "Кориснично име")]
        public string Username { get; set; }

        [Display(Name = "Емаил")]
        public string Email { get; set; }
        public string PicPath { get; set; }
        public int KorisnikId { get; set; }

        [Display(Name = "Матични број(ЈМБГ)")]
        public string MaticniBroj { get; set; }

        [Display(Name = "Бар код")]
        public string BarCode { get; set; }

        [Display(Name = "Број чланске карте")]
        public string ClanskaKartaBroj { get; set; }

        [Display(Name = "Адреса")]
        public string Adresa { get; set; }

        [Display(Name = "Место")]
        public string Mesto { get; set; }

        [Display(Name = "Број телефона")]
        public string Telefon { get; set; }

        public List<int> AccessLevels { get; set; }

        public IstorijaZaPrikaz Istorija { get; set; }

        public KorisnikZaPrikaz(Korisnik user)
        {
            Ime = user.Ime;
            Prezime = user.Prezime;
            Username = user.Username;
            Email = user.Email;
            PicPath = user.PicPath;
            KorisnikId = user.KorisnikID;
            MaticniBroj = user.MaticniBroj;
            BarCode = user.BarCode;
            ClanskaKartaBroj = user.ClanskaKartaBroj;
            Adresa = user.Adresa;
            Mesto = user.Mesto;
            Telefon = user.Telefon;
            Istorija = new IstorijaZaPrikaz();

            
            user.Na_Citanju.OrderByDescending(x => x.DatumUzimanja).Take(10).ToList().ForEach(x => { if (x != null) Istorija.NaCitanju.Add(new NaCitanjuZaPrikaz(x)); });

            user.Preuzeo.OrderByDescending(x => x.VremePreuzimanja).Take(10).ToList().ForEach(x => { if (x != null) Istorija.PreuzeteKnjige.Add(new PreuzeoZaPrikaz(x)); });
            
            user.Rezervacije.OrderByDescending(x => x.DatumPocetkaRezervacije).Take(10).ToList().ForEach(x => { if (x != null) Istorija.Rezervisane.Add(new RezervacijeZaPrikaz(x)); });

            user.Komentar.OrderBy(x => x.VoteUp - x.VoteDown).Take(10).ToList().ForEach(x => { if (x != null) Istorija.Komentari.Add(new KomentarZaPrikaz(x, true)); });

            AccessLevels = new List<int>();
            foreach (KorisnikGrupa kg in user.KorisnikGrupa.ToList())
            {
                if (kg.Grupa.AccessLevel != null)
                    AccessLevels.Add((int)kg.Grupa.AccessLevel);
            }


        }

    }
}