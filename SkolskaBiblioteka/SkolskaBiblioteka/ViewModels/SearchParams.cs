﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.ViewModels
{
    public class SearchParams
    {
        [Display(Name = "Наслов књиге: ")]
        public string Naslov { get; set; }

        [Display(Name = "Име издавача: ")]
        public string ImeIzdavaca { get; set; }

        [Display(Name = "Жанр: ")]
        public string Signatura { get; set; }

        [Display(Name = "Папирно издање: ")]
        public bool PapirnoIzdanje { get; set; }

        [Display(Name = "Име аутора: ")]
        public string ImeAutora { get; set; }

        [Display(Name = "Година издања: ")]
        [Range(0, int.MaxValue, ErrorMessage = "Мора бити број")]
        public int? GodinaIzanja { get; set; }
    }
}