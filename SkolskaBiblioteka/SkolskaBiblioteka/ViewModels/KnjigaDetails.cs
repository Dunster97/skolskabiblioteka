﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;
using PagedList;

namespace SkolskaBiblioteka.ViewModels
{
    public class KnjigaDetails
    {
        public int KnjigaId { get; set; }
        public string PicPath { get; set; }
        public string PdfPath { get; set; }
        public string Naziv { get; set; }
        public string Autori { get; set; }
        public string Izdavac { get; set; }
        public string Opis { get; set; }
        public int SlobodniPrimerci { get; set; }
        public int DownloadCounter { get; set; }
        public int CitanoCounter { get; set; }
        public int UkupnoPrimeraka { get; set; }
        public int TrenutnoCitajuCounter { get; set; }
        public bool ElektronskaKopija { get; set; }
        public bool FizickaKopija { get; set; }
        public int BrojKomentara { get; set; }

        public Komentar NoviKomentar { get; set; }



        private List<KomentarZaPrikaz> Komentari { get; set; }

        public IPagedList<KomentarZaPrikaz> PlKzp { get; set; }

        public KnjigaDetails(Knjiga k, int? p)
        {
            SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

            KnjigaId = k.KnjigaID;

            PdfPath = k.PDFPath;

            NoviKomentar = db.Komentar.Create();

            PicPath = k.PicPath;

            if (k.Naziv != null)
                Naziv = k.Naziv;
            else
                Naziv = "Непознато";

            ElektronskaKopija = false;
            if (k.PDFPath != "")
                ElektronskaKopija = true;

            FizickaKopija = false;
            if (k.PapirnoIzdanje == true)
                FizickaKopija = true;

            if (k.Autor.Count > 0)
            {
                string Autorcici = "";

                foreach (Autor a in k.Autor)
                {
                    Autorcici += ", " + a.Ime + " " + a.Prezime;
                }

                Autori = Autorcici.Remove(0, 2);
            }
            else
                Autori = "Непознато";

            if (k.Izdavac != null && k.Izdavac.Naziv != null)
                Izdavac = k.Izdavac.Naziv;
            else
                Izdavac = "Непознато";

            if (k.Opis != null)
                Opis = k.Opis;
            else
                Opis = "";

            BrojKomentara = k.Komentar.Count;

            Komentari = new List<KomentarZaPrikaz>();

            foreach (Komentar kom in k.Komentar.OrderByDescending(x => x.VoteUp - x.VoteDown))
            {

                KomentarZaPrikaz kzp = new KomentarZaPrikaz(kom, true);

                Komentari.Add(kzp);
            }

            int pageSize = 10;
            int pageNumber = (p ?? 1);
            if(pageNumber<=0)
            {
                pageNumber = 1;
            }

            PlKzp = Komentari.ToPagedList(pageNumber, pageSize);

            SlobodniPrimerci = k.Primerak.Where(x => x.Aktivna).Count();
            DownloadCounter = k.Preuzeo.Count;
            UkupnoPrimeraka = k.Primerak.Count;

            CitanoCounter = 0;
            foreach (Primerak pp in k.Primerak)
            {
                CitanoCounter += pp.Na_Citanju.Count;
                TrenutnoCitajuCounter += pp.Na_Citanju.Where(x => x.DatumVracanja == null).Count();
            }

        }

    }
}