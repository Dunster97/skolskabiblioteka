﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.ViewModels
{
    public class ObavestenjeZaPrikaz
    {
        public int KorisnikID;
        public string Tekst;


        public DateTime? PostDate;
        public string Kreator;
        public int ObavestenjeID;
        public bool Seen;
        public DateTime? SeenDate;
        public int ObavestenjeKorisnikID;

        public ObavestenjeZaPrikaz(ObavestenjeKorisnik k)
        {
            this.KorisnikID = k.KorisnikID;
            this.Tekst = k.Obavestenje.Tekst;
            this.PostDate = k.DatumSlanja;
            this.Kreator = k.Obavestenje.Kreator;
            this.ObavestenjeID = k.ObavestenjeID;
            if (k.Seen == null || k.Seen == false)
                this.Seen = false;
            else
                this.Seen = true;
            this.SeenDate = k.SeenDate;
            this.ObavestenjeKorisnikID = k.ObavestenjeKorisnikID;
        }

    }
}