﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.ViewModels
{
    public class ReportZaPrikaz
    {

        public KomentarZaPrikaz kzp { get; set; }
        public PrijavaKomentara report { get; set; }

    }
}