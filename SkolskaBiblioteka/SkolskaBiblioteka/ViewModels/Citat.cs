﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.ViewModels
{
    public class Citat
    {

        public string Text { get; set; }
        public string Creator { get; set; }

        public Citat(string Text, string Creator)
        {
            this.Text = Text;
            this.Creator = Creator;
        }

    }
}