﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.ViewModels
{
    public class KomentarZaPrikaz : Komentar
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string PicPath { get; set; }
        public string Naslov { get; set; }
        public bool AdditionalInfo { get; set; }

        public KomentarZaPrikaz(Komentar k, bool AI)
        {
            KomentarID = k.KomentarID;
            KnjigaID = k.KnjigaID;
            KomentarText = k.KomentarText;
            KorisnikID = k.KorisnikID;
            Reported = k.Reported;
            VremeKomentara = k.VremeKomentara;
            Ime = k.Korisnik.Ime;
            Prezime = k.Korisnik.Prezime;
            PicPath = k.Korisnik.PicPath;
            VoteUp = k.VoteUp;
            VoteDown = k.VoteDown;
            Naslov = k.Knjiga.Naziv;
            AdditionalInfo = AI;
        }
    }
}