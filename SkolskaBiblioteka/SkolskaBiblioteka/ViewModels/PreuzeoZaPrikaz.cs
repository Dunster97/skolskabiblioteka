﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;
using System.ComponentModel.DataAnnotations;

namespace SkolskaBiblioteka.ViewModels
{
    public class PreuzeoZaPrikaz
    {
        [Display(Name = "Наслов")]
        public string NazivKnjige { get; set; }

        [Display(Name = "Датум и време преузимања")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VremePreuzimanja { get; set; }

        public PreuzeoZaPrikaz(Preuzeo p)
        {
            NazivKnjige = p.Knjiga.Naziv;
            VremePreuzimanja = p.VremePreuzimanja;
        }
    }
}