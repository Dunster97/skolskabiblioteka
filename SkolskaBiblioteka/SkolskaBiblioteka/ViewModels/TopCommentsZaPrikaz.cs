﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.ViewModels
{
    public class TopCommentsZaPrikaz
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int KorisnikId { get; set; }
        public string PicPath { get; set; }
        public string Text { get; set; }
        public int KnjigaID { get; set; }
        public string Naslov { get; set; }
        public int? VoteUp { get; set; }
        public int? VoteDown { get; set; }

        public TopCommentsZaPrikaz(Komentar k)
        {
            Ime = k.Korisnik.Ime;
            Prezime = k.Korisnik.Prezime;
            KorisnikId = k.Korisnik.KorisnikID;
            PicPath = k.Korisnik.PicPath;
            if (!String.IsNullOrEmpty(k.KomentarText))
            {
                if (k.KomentarText.Length > 150)
                    Text = k.KomentarText.Substring(0, 150) + "...";
                else
                    Text = k.KomentarText;
            }
            KnjigaID = k.KnjigaID;
            Naslov = k.Knjiga.Naziv;
            VoteUp = k.VoteUp;
            VoteDown = k.VoteDown;
        }
    }
}