﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SkolskaBiblioteka.Areas.Admin.Models
{
    public class CreateBookViewModel
    {
        [Display(Name = "УДК")]
        [StringLength(50)]
        public string UDK { get; set; }

        [Display(Name = "ИСБН")]
        [StringLength(50)]
        public string ISBN { get; set; }

        [Required]
        [Display(Name = "Назив књиге")]
        [StringLength(100)]
        public string Naziv { get; set; }

        public int? SignaturaID { get; set; }
        public List<SelectListItem> Signature { get; set; }

        [Display(Name = "Путања до слике")]
        public string PicPath { get; set; }

        public int? IzdavacID { get; set; }
        public List<SelectListItem> Izdavaci { get; set; }

        [Display(Name = "Датум издавања")]
        public DateTime? DatumIzdavanja { get; set; }

        [Display(Name = "Линк за преузимање")]
        public string PDFPath { get; set; }

        [Display(Name = "Да ли постоји у папирној верзији")]
        public bool? PapirnoIzdanje { get; set; }

        [Display(Name = "Опис књиге")]
        public string Opis { get; set; }

        [Display(Name = "Аутори")]
        public string Autori { get; set; }

        public List<SelectListItem> ListaAutora { get; set; }

        public int? Autor1 { get; set; }
        public int? Autor2 { get; set; }
        public int? Autor3 { get; set; }
        public int? Autor4 { get; set; }

        public CreateBookViewModel() { }
        public CreateBookViewModel(Knjiga k)
        {
            UDK = k.UDK;
            ISBN = k.ISBN;
            Naziv = k.Naziv;
            SignaturaID = k.SignaturaID;
            IzdavacID = k.IzdavacID;
            DatumIzdavanja = k.DatumIzdavanja;
            PDFPath = k.PDFPath;
            PapirnoIzdanje = k.PapirnoIzdanje;
            Opis = k.Opis;
            PicPath = k.PicPath;
        }
    }
}