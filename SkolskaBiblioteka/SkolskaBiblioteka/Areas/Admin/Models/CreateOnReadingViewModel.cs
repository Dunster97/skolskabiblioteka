﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkolskaBiblioteka.Areas.Admin.Models
{
    public class CreateOnReadingViewModel
    {
        public string Username { get; set; }

        public string BarCode { get; set; }

        public string InventarskiBroj { get; set; }

        public int RokZaVracanje { get; set; }
    }
}