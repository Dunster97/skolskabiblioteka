﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkolskaBiblioteka.ViewModels;
using System.Web.Routing;

namespace SkolskaBiblioteka.Areas.Admin
{
    public class AdminAuth: AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
                return false;  // if user not Authenticated, do not allow

            KorisnikZaPrikaz kzp = (KorisnikZaPrikaz)httpContext.Session["Korisnik"];
            if (kzp != null)
            {
                if (kzp.AccessLevels.Count == 0) return false;
                if (kzp.AccessLevels.Min() == 7)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Korisnik kor = new SkolskaBibliotekaDBModel().Korisnik.FirstOrDefault(k => k.Username == httpContext.User.Identity.Name);

                if(kor != null)
                {
                    kzp = new KorisnikZaPrikaz(kor);
                    if (kzp.AccessLevels.Count == 0) return false;

                    if (kzp.AccessLevels.Min() == 7)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary 
                {
                    { "action", "Index" },
                    { "controller", "Home" },
                    { "area", ""}
                });
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary 
                {
                    { "action", "Login" },
                    { "controller", "Account" },
                    { "area", "" }
                });
            }
        }
    }
}