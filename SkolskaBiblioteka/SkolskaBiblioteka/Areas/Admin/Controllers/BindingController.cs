﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class BindingController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Binding
        public ActionResult Index(int? p)
        {
            var povezi = db.Povezi.OrderBy(x => x.VrstaPovezaID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = povezi.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(povezi.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Binding/Details/5
        public ActionResult Details(int id)
        {
            Povezi povez = db.Povezi.FirstOrDefault(p => p.VrstaPovezaID == id);

            return View(povez);
        }

        // GET: Admin/Binding/Create
        public ActionResult Create()
        {
            Povezi p = new Povezi();

            return View(p);
        }

        // POST: Admin/Binding/Create
        [HttpPost]
        public ActionResult Create(Povezi p)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    db.Povezi.Add(p);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                Povezi po = new Povezi();

                return View(po);
            }
        }

        // GET: Admin/Binding/Edit/5
        public ActionResult Edit(int id)
        {
            Povezi povez = db.Povezi.FirstOrDefault(p => p.VrstaPovezaID == id);

            return View(povez);
        }

        // POST: Admin/Binding/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Povezi pov)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    Povezi povez = db.Povezi.FirstOrDefault(p => p.VrstaPovezaID == id);
                    povez.Naziv = pov.Naziv;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Binding/Delete/5
        public ActionResult Delete(int id)
        {
            Povezi povez = db.Povezi.FirstOrDefault(p => p.VrstaPovezaID == id);

            return View(povez);
        }

        // POST: Admin/Binding/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection data)
        {
            try
            {
                Povezi povez = db.Povezi.FirstOrDefault(p => p.VrstaPovezaID == id);
                if(povez != null)
                {
                    db.Povezi.Remove(povez);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
