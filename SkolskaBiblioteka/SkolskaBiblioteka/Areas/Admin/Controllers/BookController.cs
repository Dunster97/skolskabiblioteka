﻿using SkolskaBiblioteka.Areas.Admin.Models;
using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class BookController : Controller
    {
        public SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Book
        public ActionResult Index(int? p)
        {
            var knjige = db.Knjiga.OrderBy(k => k.KnjigaID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = knjige.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(knjige.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Book/Details/5
        public ActionResult Details(int id)
        {
            Knjiga knjiga = db.Knjiga.FirstOrDefault(k => k.KnjigaID == id);

            if (knjiga != null)
                return View(knjiga);
            else
                return RedirectToAction("Index");
        }

        // GET: Admin/Book/Create
        public ActionResult Create()
        {
            CreateBookViewModel knjiga = new CreateBookViewModel();
            knjiga.Izdavaci = db.Izdavac.Select(f => new SelectListItem { Value = f.IzdavacID.ToString(), Text = f.Naziv }).ToList();
            knjiga.Signature = db.Signatura.Select(f => new SelectListItem { Value = f.SignaturaID.ToString(), Text = f.Naziv }).ToList();
            knjiga.ListaAutora = new List<SelectListItem>();
            knjiga.ListaAutora.Add(new SelectListItem { Value = "", Text = "" });
            knjiga.ListaAutora.AddRange(db.Autor.Select(f => new SelectListItem { Value = f.AutorID.ToString(), Text = f.Ime + " " + f.Prezime }).ToList());

            return View(knjiga);
        }

        // POST: Admin/Book/Create
        [HttpPost]
        public ActionResult Create(CreateBookViewModel novaKnjiga)
        {
            try
            {
                //if(ModelState.IsValid)
                //{
                Knjiga k = new Knjiga();
                k.UDK = novaKnjiga.UDK;
                k.ISBN = novaKnjiga.ISBN;
                k.Naziv = novaKnjiga.Naziv;
                k.SignaturaID = novaKnjiga.SignaturaID;
                k.PicPath = novaKnjiga.PicPath;
                k.IzdavacID = novaKnjiga.IzdavacID;
                k.DatumIzdavanja = novaKnjiga.DatumIzdavanja;
                k.PDFPath = novaKnjiga.PDFPath;
                k.PapirnoIzdanje = novaKnjiga.PapirnoIzdanje;
                k.Opis = novaKnjiga.Opis;

                // TODO: Naci bolji nacin...
                if (novaKnjiga.Autor1 != 0)
                {
                    Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == novaKnjiga.Autor1);
                    if (aut != null)
                    {
                        k.Autor.Add(aut);
                    }
                }

                if (novaKnjiga.Autor2 != 0 && novaKnjiga.Autor2 != novaKnjiga.Autor1)
                {
                    Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == novaKnjiga.Autor2);
                    if (aut != null)
                    {
                        k.Autor.Add(aut);
                    }
                }

                if (novaKnjiga.Autor3 != 0 && novaKnjiga.Autor3 != novaKnjiga.Autor2 && novaKnjiga.Autor3 != novaKnjiga.Autor1)
                {
                    Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == novaKnjiga.Autor3);
                    if (aut != null)
                    {
                        k.Autor.Add(aut);
                    }
                }

                if (novaKnjiga.Autor4 != 0 && novaKnjiga.Autor4 != novaKnjiga.Autor3 && novaKnjiga.Autor4 != novaKnjiga.Autor2 && novaKnjiga.Autor4 != novaKnjiga.Autor1)
                {
                    Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == novaKnjiga.Autor4);
                    if (aut != null)
                    {
                        k.Autor.Add(aut);
                    }
                }

                db.Knjiga.Add(k);
                db.SaveChanges();
                //}

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Book/Edit/5
        public ActionResult Edit(int id)
        {
            Knjiga knjiga = db.Knjiga.FirstOrDefault(k => k.KnjigaID == id);
            CreateBookViewModel knjigaV = new CreateBookViewModel(knjiga);
            knjigaV.Izdavaci = db.Izdavac.Select(f => new SelectListItem { Value = f.IzdavacID.ToString(), Text = f.Naziv }).ToList();
            knjigaV.Signature = db.Signatura.Select(f => new SelectListItem { Value = f.SignaturaID.ToString(), Text = f.Naziv }).ToList();
            knjigaV.Autor1 = 0;
            knjigaV.Autor2 = 0;
            knjigaV.Autor3 = 0;
            knjigaV.Autor4 = 0;

            if (knjiga.Autor.Count == 1)
            {
                knjigaV.Autor1 = knjiga.Autor.ElementAt(0).AutorID;
            }
            else if (knjiga.Autor.Count == 2)
            {
                knjigaV.Autor1 = knjiga.Autor.ElementAt(0).AutorID;
                knjigaV.Autor2 = knjiga.Autor.ElementAt(1).AutorID;
            }
            else if (knjiga.Autor.Count == 3)
            {
                knjigaV.Autor1 = knjiga.Autor.ElementAt(0).AutorID;
                knjigaV.Autor2 = knjiga.Autor.ElementAt(1).AutorID;
                knjigaV.Autor3 = knjiga.Autor.ElementAt(2).AutorID;
            }
            else if (knjiga.Autor.Count == 4)
            {
                knjigaV.Autor1 = knjiga.Autor.ElementAt(0).AutorID;
                knjigaV.Autor2 = knjiga.Autor.ElementAt(1).AutorID;
                knjigaV.Autor3 = knjiga.Autor.ElementAt(2).AutorID;
                knjigaV.Autor4 = knjiga.Autor.ElementAt(3).AutorID;
            }

            knjigaV.ListaAutora = new List<SelectListItem>();
            knjigaV.ListaAutora.Add(new SelectListItem { Value = "", Text = "" });
            knjigaV.ListaAutora.AddRange(db.Autor.Select(f => new SelectListItem { Value = f.AutorID.ToString(), Text = f.Ime + " " + f.Prezime }).ToList());

            return View(knjigaV);
        }

        // POST: Admin/Book/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CreateBookViewModel knjiga)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Knjiga k = db.Knjiga.FirstOrDefault(kn => kn.KnjigaID == id);
                    k.UDK = knjiga.UDK;
                    k.ISBN = knjiga.ISBN;
                    k.Naziv = knjiga.Naziv;
                    k.SignaturaID = knjiga.SignaturaID;
                    k.PicPath = knjiga.PicPath;
                    k.IzdavacID = knjiga.IzdavacID;
                    k.DatumIzdavanja = knjiga.DatumIzdavanja;
                    k.PDFPath = knjiga.PDFPath;
                    k.PapirnoIzdanje = knjiga.PapirnoIzdanje;
                    k.Opis = knjiga.Opis;

                    // TODO: Naci bolji nacin...
                    k.Autor.Clear();
                    if (knjiga.Autor1 != 0)
                    {
                        Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == knjiga.Autor1);
                        if (aut != null)
                        {
                            k.Autor.Add(aut);
                        }
                    }

                    if (knjiga.Autor2 != 0 && knjiga.Autor2 != knjiga.Autor1)
                    {
                        Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == knjiga.Autor2);
                        if (aut != null)
                        {
                            k.Autor.Add(aut);
                        }
                    }

                    if (knjiga.Autor3 != 0 && knjiga.Autor3 != knjiga.Autor2 && knjiga.Autor3 != knjiga.Autor1)
                    {
                        Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == knjiga.Autor3);
                        if (aut != null)
                        {
                            k.Autor.Add(aut);
                        }
                    }

                    if (knjiga.Autor4 != 0 && knjiga.Autor4 != knjiga.Autor3 && knjiga.Autor4 != knjiga.Autor2 && knjiga.Autor4 != knjiga.Autor1)
                    {
                        Autor aut = db.Autor.FirstOrDefault(a => a.AutorID == knjiga.Autor4);
                        if (aut != null)
                        {
                            k.Autor.Add(aut);
                        }
                    }

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Book/Delete/5
        public ActionResult Delete(int id)
        {
            Knjiga knjiga = db.Knjiga.FirstOrDefault(k => k.KnjigaID == id);

            return View(knjiga);
        }

        // POST: Admin/Book/Delete/5
        [HttpPost]
        public ActionResult Delete(int KnjigaID, FormCollection form)
        {
            try
            {
                Knjiga knjiga = db.Knjiga.FirstOrDefault(k => k.KnjigaID == KnjigaID);
                if (knjiga != null)
                {
                    db.Knjiga.Remove(knjiga);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}
