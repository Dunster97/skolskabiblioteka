﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class ReservationController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Reservation
        public ActionResult Index(int? p)
        {
            var rez = db.Rezervacije.OrderByDescending(r => r.RezervacijeID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Active(int? p)
        {
            var rez = db.Rezervacije.Where(r => r.Validna).OrderByDescending(r => r.RezervacijeID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Search(string username, int? p)
        {
            var rez = db.Rezervacije.Where(r => r.Validna && r.Korisnik.Username.ToLower().Contains(username.ToLower())).OrderByDescending(r => r.RezervacijeID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            ViewBag.Username = (username == null) ? "" : username;

            return View(rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Inactive(int? p)
        {
            var rez = db.Rezervacije.Where(r => !r.Validna).OrderByDescending(r => r.RezervacijeID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(rez.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Reservation/Details/5
        public ActionResult Details(int id)
        {
            Rezervacije rez = db.Rezervacije.FirstOrDefault(r => r.RezervacijeID == id);

            return View(rez);
        }

        // GET: Admin/Reservation/Edit/5
        public ActionResult Edit(int id)
        {
            Rezervacije rez = db.Rezervacije.FirstOrDefault(r => r.RezervacijeID == id);
            ViewBag.Knjige = db.Knjiga.Select(f => new SelectListItem { Value = f.KnjigaID.ToString(), Text = f.Naziv }).ToList();
            ViewBag.Korisnici = db.Korisnik.OrderBy(x => x.Prezime).Select(f => new SelectListItem { Value = f.KorisnikID.ToString(), Text = f.Ime + " " + f.Prezime }).ToList();
            ViewBag.Primerci = db.Primerak.Where(x => x.KnjigaID == rez.KnjigaID).Select(f => new SelectListItem { Value = f.PrimerakID.ToString(), Text = f.InventarskiBroj + " " + f.Povezi.Naziv }).ToList();

            return View(rez);
        }

        // POST: Admin/Reservation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Rezervacije rezer)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Rezervacije rez = db.Rezervacije.FirstOrDefault(r => r.RezervacijeID == id);
                    if(rez != null)
                    {
                        rez.KnjigaID = rezer.KnjigaID;
                        rez.KorisnikID = rezer.KorisnikID;
                        rez.PrimerakID = rezer.PrimerakID;
                        rez.Validna = rezer.Validna;
                        rez.DatumPocetkaRezervacije = rezer.DatumPocetkaRezervacije;
                        rez.DatumIstekaRezervacije = rezer.DatumIstekaRezervacije;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Reservation/Delete/5
        public ActionResult Delete(int id)
        {
            Rezervacije rez = db.Rezervacije.FirstOrDefault(r => r.RezervacijeID == id);

            return View(rez);
        }

        // POST: Admin/Reservation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Rezervacije rez = db.Rezervacije.FirstOrDefault(r => r.RezervacijeID == id);
                if(rez != null)
                {
                    db.Rezervacije.Remove(rez);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
