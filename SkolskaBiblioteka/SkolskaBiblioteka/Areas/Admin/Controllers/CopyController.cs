﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class CopyController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Copy
        public ActionResult Index(int? p)
        {
            var primerci = db.Primerak.OrderBy(x => x.PrimerakID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = primerci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(primerci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Search(string naziv, string invbr, string barkod, int? p)
        {
            var primerci = db.Primerak.OrderByDescending(x => x.PrimerakID);
            if(!String.IsNullOrWhiteSpace(naziv))
            {
                primerci = primerci.Where(x => x.Knjiga.Naziv.ToLower().Contains(naziv.ToLower())).OrderByDescending(x => x.PrimerakID);
            }

            if (!String.IsNullOrWhiteSpace(invbr))
            {
                primerci = primerci.Where(x => x.InventarskiBroj.ToLower() == invbr.ToLower()).OrderByDescending(x => x.PrimerakID);
            }

            if (!String.IsNullOrWhiteSpace(barkod))
            {
                primerci = primerci.Where(x => x.BarCode.ToLower() == barkod.ToLower()).OrderByDescending(x => x.PrimerakID);
            }

            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = primerci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            ViewBag.Naziv = (naziv == null) ? "" : naziv;
            ViewBag.InvBr = (invbr == null) ? "" : invbr;
            ViewBag.BarCode = (barkod == null) ? "" : barkod;

            return View(primerci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Copy/Details/5
        public ActionResult Details(int id)
        {
            Primerak pr = db.Primerak.FirstOrDefault(p => p.PrimerakID == id);

            return View(pr);
        }

        public JsonResult GetAllByBook(string knjigaID)
        {
            List<SelectListItem> primerci = db.Primerak.Where(p => p.KnjigaID.ToString() == knjigaID)
                                                        .Select(f => new SelectListItem {
                                                            Value = f.PrimerakID.ToString(), Text = f.InventarskiBroj + " " + f.Povezi.Naziv 
                                                        })
                                                        .ToList();

            return Json(new SelectList(primerci, "Value", "Text"));
        }

        // GET: Admin/Copy/Create
        public ActionResult Create()
        {
            Primerak pr = new Primerak();
            ViewBag.Knjige = db.Knjiga.Select(f => new SelectListItem { Value = f.KnjigaID.ToString(), Text = f.Naziv }).ToList();
            ViewBag.Povezi = db.Povezi.Select(f => new SelectListItem { Value = f.VrstaPovezaID.ToString(), Text = f.Naziv }).ToList();

            return View(pr);
        }

        // POST: Admin/Copy/Create
        [HttpPost]
        public ActionResult Create(Primerak p)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.Primerak.Add(p);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Copy/Edit/5
        public ActionResult Edit(int id)
        {
            Primerak pr = db.Primerak.FirstOrDefault(p => p.PrimerakID == id);
            ViewBag.Knjige = db.Knjiga.Select(f => new SelectListItem { Value = f.KnjigaID.ToString(), Text = f.Naziv }).ToList();
            ViewBag.Povezi = db.Povezi.Select(f => new SelectListItem { Value = f.VrstaPovezaID.ToString(), Text = f.Naziv }).ToList();

            return View(pr);
        }

        // POST: Admin/Copy/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Primerak px)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Primerak pr = db.Primerak.FirstOrDefault(p => p.PrimerakID == id);
                    pr.KnjigaID = px.KnjigaID;
                    pr.InventarskiBroj = px.InventarskiBroj;
                    pr.VrstaPovezaID = px.VrstaPovezaID;
                    pr.Aktivna = px.Aktivna;
                    pr.Depo = px.Depo;
                    pr.BarCode = px.BarCode;

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Copy/Delete/5
        public ActionResult Delete(int id)
        {
            Primerak pr = db.Primerak.FirstOrDefault(p => p.PrimerakID == id);

            return View(pr);
        }

        // POST: Admin/Copy/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection data)
        {
            try
            {
                Primerak primerak = db.Primerak.FirstOrDefault(p => p.PrimerakID == id);
                if(primerak != null)
                {
                    db.Primerak.Remove(primerak);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
