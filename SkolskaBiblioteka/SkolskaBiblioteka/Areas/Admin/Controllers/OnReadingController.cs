﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class OnReadingController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/OnReading
        public ActionResult Index(int? p)
        {
            var na_citanju = db.Na_Citanju.OrderByDescending(n => n.Na_CitanjuID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/OnReading/Late
        public ActionResult Late(int? p)
        {
            var na_citanju = db.Na_Citanju.OrderByDescending(n => n.Na_CitanjuID).Where(nc => nc.DatumVracanja == null && nc.RokZaVracanje < DateTime.Now);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Search(int? p, string username)
        {
            var na_citanju = db.Na_Citanju.Where(n => n.Korisnik.Username.ToLower() == username.ToLower()).OrderByDescending(n => n.Na_CitanjuID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(na_citanju.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/OnReading/Details/5
        public ActionResult Details(int id)
        {
            Na_Citanju nc = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);

            return View(nc);
        }

        [HttpPost]
        public ActionResult Return(int id)
        {
            Na_Citanju nc = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);
            if(nc != null)
            {
                if (nc.DatumVracanja == null)
                {
                    nc.DatumVracanja = DateTime.Now;
                    nc.Primerak.Aktivna = true;

                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }

        // POST: Admin/OnReading/Create
        [HttpPost]
        public ActionResult Create(int? primerakId, string username)
        {
            try
            {
                if(primerakId != null && username != null)
                {
                    Primerak pr = db.Primerak.FirstOrDefault(p => p.PrimerakID == primerakId);
                    Korisnik kor = db.Korisnik.FirstOrDefault(k => k.Username == username);

                    if (pr != null && kor != null)
                    {
                        if(pr.Aktivna == false)
                        {
                            if(kor.Rezervacije.FirstOrDefault(x=>x.PrimerakID==primerakId)==null)
                                return RedirectToAction("Index");
                        }

                        pr.Aktivna = false;

                        Na_Citanju nc = new Na_Citanju();
                        nc.KorisnikID = kor.KorisnikID;
                        nc.PrimerakID = pr.PrimerakID;
                        nc.DatumUzimanja = DateTime.Now;
                        nc.RokZaVracanje = DateTime.Now.AddDays(21);

                        var rez = db.Rezervacije.FirstOrDefault(x => x.Korisnik.Username == username && x.PrimerakID == primerakId);
                        if (rez != null)
                        {
                            rez.Validna = false;
                        }

                        db.Na_Citanju.Add(nc);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/OnReading/Edit/5
        public ActionResult Edit(int id)
        {
            Na_Citanju nc = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);
            ViewBag.Primerci = db.Primerak.Where(p => p.KnjigaID == nc.Primerak.KnjigaID).Select(f => new SelectListItem { Value = f.PrimerakID.ToString(), Text = f.InventarskiBroj + " - " + f.Povezi.Naziv }).ToList();
            ViewBag.Korisnici = db.Korisnik.OrderBy(k => k.Prezime).Select(f => new SelectListItem { Value = f.KorisnikID.ToString(), Text = f.Ime + " - " + f.Prezime }).ToList();

            return View(nc);
        }

        // POST: Admin/OnReading/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Na_Citanju nc)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Na_Citanju na = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);
                    if(na != null)
                    {
                        na.KorisnikID = nc.KorisnikID;
                        na.PrimerakID = nc.PrimerakID;
                        na.RokZaVracanje = nc.RokZaVracanje;
                        na.DatumUzimanja = nc.DatumUzimanja;
                        na.DatumVracanja = nc.DatumVracanja;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/OnReading/Delete/5
        public ActionResult Delete(int id)
        {
            Na_Citanju nc = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);

            return View(nc);
        }

        // POST: Admin/OnReading/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Na_Citanju nc = db.Na_Citanju.FirstOrDefault(n => n.Na_CitanjuID == id);
                if(nc != null)
                {
                    db.Na_Citanju.Remove(nc);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
