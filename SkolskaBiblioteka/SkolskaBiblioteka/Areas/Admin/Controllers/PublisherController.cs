﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class PublisherController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Publisher
        public ActionResult Index(int? p)
        {
            var izdavaci = db.Izdavac.OrderBy(x => x.IzdavacID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = izdavaci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(izdavaci.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Publisher/Details/5
        public ActionResult Details(int id)
        {
            Izdavac izdavac = db.Izdavac.FirstOrDefault(i => i.IzdavacID == id);

            return View(izdavac);
        }

        // GET: Admin/Publisher/Create
        public ActionResult Create()
        {
            Izdavac izdavac = new Izdavac();

            return View(izdavac);
        }

        // POST: Admin/Publisher/Create
        [HttpPost]
        public ActionResult Create(Izdavac izdavac)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.Izdavac.Add(izdavac);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                Izdavac i = new Izdavac();

                return View(i);
            }
        }

        // GET: Admin/Publisher/Edit/5
        public ActionResult Edit(int id)
        {
            Izdavac izdavac = db.Izdavac.FirstOrDefault(i => i.IzdavacID == id);

            return View(izdavac);
        }

        // POST: Admin/Publisher/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Izdavac izdavac)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Izdavac i = db.Izdavac.FirstOrDefault(x => x.IzdavacID == id);
                    i.Naziv = izdavac.Naziv;

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                Izdavac i = new Izdavac();

                return View(i);
            }
        }

        // GET: Admin/Publisher/Delete/5
        public ActionResult Delete(int id)
        {
            Izdavac izdavac = db.Izdavac.FirstOrDefault(i => i.IzdavacID == id);

            return View(izdavac);
        }

        // POST: Admin/Publisher/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection data)
        {
            try
            {
                Izdavac izdavac = db.Izdavac.FirstOrDefault(i => i.IzdavacID == id);
                if(izdavac != null)
                {
                    db.Izdavac.Remove(izdavac);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
