﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SkolskaBibliotekaDBContext;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class NotificationController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Notification
        public ActionResult Index(int? p)
        {
            var poruke = db.Obavestenje.OrderByDescending(x => x.ObavestenjeID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Notification/Details/5
        public ActionResult Details(int id)
        {
            Obavestenje ob = db.Obavestenje.FirstOrDefault(o => o.ObavestenjeID == id);

            return View(ob);
        }

        // GET: Admin/Notification/Create
        public ActionResult Create()
        {
            Obavestenje o = new Obavestenje();

            return View(o);
        }

        // POST: Admin/Notification/Create
        [HttpPost]
        public ActionResult Create(Obavestenje ob)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ob.PostDate = DateTime.Now;
                    ob.Kreator = User.Identity.Name;

                    db.Obavestenje.Add(ob);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Notification/Edit/5
        public ActionResult Edit(int id)
        {
            Obavestenje ob = db.Obavestenje.FirstOrDefault(o => o.ObavestenjeID == id);

            return View(ob);
        }

        // POST: Admin/Notification/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Obavestenje oba)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Obavestenje ob = db.Obavestenje.FirstOrDefault(o => o.ObavestenjeID == id);
                    if(ob != null)
                    {
                        ob.Tekst = oba.Tekst;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Notification/Delete/5
        public ActionResult Delete(int id)
        {
            Obavestenje ob = db.Obavestenje.FirstOrDefault(o => o.ObavestenjeID == id);

            return View(ob);
        }

        // POST: Admin/Notification/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Obavestenje ob = db.Obavestenje.FirstOrDefault(o => o.ObavestenjeID == id);
                if(ob != null)
                {
                    db.Obavestenje.Remove(ob);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
