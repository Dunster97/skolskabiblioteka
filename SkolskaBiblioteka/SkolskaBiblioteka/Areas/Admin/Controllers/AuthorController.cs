﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class AuthorController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Author
        public ActionResult Index(int? p)
        {
            var autori = db.Autor.OrderBy(a => a.AutorID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = autori.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(autori.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Author/Details/5
        public ActionResult Details(int id)
        {
            Autor autor = db.Autor.FirstOrDefault(a => a.AutorID == id);

            return View(autor);
        }

        // GET: Admin/Author/Create
        public ActionResult Create()
        {
            Autor autor = new Autor();

            return View(autor);
        }

        // POST: Admin/Author/Create
        [HttpPost]
        public ActionResult Create(Autor autor)
        {
            try
            {
                // TODO: Add insert logic here
                if(ModelState.IsValid)
                {
                    db.Autor.Add(autor);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                Autor a = new Autor();

                return View(a);
            }
        }

        // GET: Admin/Author/Edit/5
        public ActionResult Edit(int id)
        {
            Autor autor = db.Autor.FirstOrDefault(a => a.AutorID == id);

            return View(autor);
        }

        // POST: Admin/Author/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Autor autor)
        {
            try
            {
                Autor a = db.Autor.FirstOrDefault(x => x.AutorID == id);
                if(a != null)
                {
                    a.Ime = autor.Ime;
                    a.Prezime = autor.Prezime;
                    a.DatumRodjenja = autor.DatumRodjenja;
                    a.Adresa = autor.Adresa;

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Author/Delete/5
        public ActionResult Delete(int id)
        {
            Autor autor = db.Autor.FirstOrDefault(a => a.AutorID == id);

            return View(autor);
        }

        // POST: Admin/Author/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection data)
        {
            try
            {
                // TODO: Add delete logic here
                Autor autor = db.Autor.FirstOrDefault(a => a.AutorID == id);
                if(autor != null)
                {
                    db.Autor.Remove(autor);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
