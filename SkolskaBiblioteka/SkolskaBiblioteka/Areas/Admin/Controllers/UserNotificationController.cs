﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class UserNotificationController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/UserNotification
        public ActionResult Index(int? p)
        {
            var poruke = db.ObavestenjeKorisnik.OrderByDescending(x => x.ObavestenjeKorisnikID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Admin(int? p)
        {
            var poruke = db.ObavestenjeKorisnik.Where(x => x.Korisnik.Username == "admin").OrderByDescending(x => x.ObavestenjeKorisnikID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(poruke.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/UserNotification/Details/5
        public ActionResult Details(int id)
        {
            ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.FirstOrDefault(o => o.ObavestenjeKorisnikID == id);

            return View(ok);
        }

        // GET: Admin/UserNotification/Send
        public ActionResult Send()
        {
            ObavestenjeKorisnik ok = new ObavestenjeKorisnik();
            ViewBag.Korisnici = db.Korisnik.Select(f => new SelectListItem { Value = f.KorisnikID.ToString(), Text = f.Username + " | " + f.Ime + " " + f.Prezime }).ToList();
            ViewBag.Obavestenja = db.Obavestenje.Select(f => new SelectListItem { Value = f.ObavestenjeID.ToString(), Text = f.Tekst }).ToList();

            return View(ok);
        }

        // POST: Admin/UserNotification/Send
        [HttpPost]
        public ActionResult Send(ObavestenjeKorisnik ok)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    ok.Seen = false;
                    ok.DatumSlanja = DateTime.Now;
                    db.ObavestenjeKorisnik.Add(ok);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/UserNotification/SendAll
        public ActionResult SendAll()
        {
            ObavestenjeKorisnik ok = new ObavestenjeKorisnik();
            ViewBag.Obavestenja = db.Obavestenje.Select(f => new SelectListItem { Value = f.ObavestenjeID.ToString(), Text = f.Tekst }).ToList();

            return View(ok);
        }

        // POST: Admin/UserNotification/SendAll
        [HttpPost]
        public ActionResult SendAll(ObavestenjeKorisnik ok)
        {
            try
            {
                foreach(var k in db.Korisnik.Where(x => x.Username != "admin" && x.ObavestenjeKorisnik.FirstOrDefault(z => z.ObavestenjeID == ok.ObavestenjeID) != null))
                {
                    ObavestenjeKorisnik novo = new ObavestenjeKorisnik();
                    ok.DatumSlanja = DateTime.Now;
                    ok.ObavestenjeID = ok.ObavestenjeID;
                    ok.KorisnikID = k.KorisnikID;

                    db.ObavestenjeKorisnik.Add(novo);
                }

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/UserNotification/Edit/5
        public ActionResult Edit(int id)
        {
            ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.FirstOrDefault(o => o.ObavestenjeKorisnikID == id);
            ViewBag.Korisnici = db.Korisnik.Select(f => new SelectListItem { Value = f.KorisnikID.ToString(), Text = f.Username + " | " + f.Ime + " " + f.Prezime }).ToList();
            ViewBag.Obavestenja = db.Obavestenje.Select(f => new SelectListItem { Value = f.ObavestenjeID.ToString(), Text = f.Tekst }).ToList();

            return View(ok);
        }

        // POST: Admin/UserNotification/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ObavestenjeKorisnik ok2)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.FirstOrDefault(o => o.ObavestenjeKorisnikID == id);
                    if(ok != null)
                    {
                        ok.ObavestenjeID = ok2.ObavestenjeID;
                        ok.KorisnikID = ok2.KorisnikID;
                        ok.Seen = ok2.Seen;
                        ok.SeenDate = ok2.SeenDate;
                        ok.DatumSlanja = ok2.DatumSlanja;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/UserNotification/Delete/5
        public ActionResult Delete(int id)
        {
            ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.FirstOrDefault(o => o.ObavestenjeKorisnikID == id);

            return View(ok);
        }

        // POST: Admin/UserNotification/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.FirstOrDefault(o => o.ObavestenjeKorisnikID == id);
                if(ok != null)
                {
                    db.ObavestenjeKorisnik.Remove(ok);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
