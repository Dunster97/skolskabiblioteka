﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{ 
    [AdminAuth]
    public class GenreController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Genre
        public ActionResult Index(int? p)
        {
            var signature = db.Signatura.OrderBy(x => x.SignaturaID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = signature.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(signature.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Genre/Details/5
        public ActionResult Details(int id)
        {
            Signatura sig = db.Signatura.FirstOrDefault(s => s.SignaturaID == id);

            return View(sig);
        }

        // GET: Admin/Genre/Create
        public ActionResult Create()
        {
            Signatura sig = new Signatura();

            return View(sig);
        }

        // POST: Admin/Genre/Create
        [HttpPost]
        public ActionResult Create(Signatura sig)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.Signatura.Add(sig);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Genre/Edit/5
        public ActionResult Edit(int id)
        {
            Signatura sig = db.Signatura.FirstOrDefault(s => s.SignaturaID == id);

            return View(sig);
        }

        // POST: Admin/Genre/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Signatura signatura)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Signatura sig = db.Signatura.FirstOrDefault(s => s.SignaturaID == id);
                    sig.Naziv = signatura.Naziv;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                Signatura sig = new Signatura();

                return View(sig);
            }
        }

        // GET: Admin/Genre/Delete/5
        public ActionResult Delete(int id)
        {
            Signatura sig = db.Signatura.FirstOrDefault(s => s.SignaturaID == id);

            return View(sig);
        }

        // POST: Admin/Genre/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Signatura signatura = db.Signatura.FirstOrDefault(s => s.SignaturaID == id);
                if(signatura != null)
                {
                    db.Signatura.Remove(signatura);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
