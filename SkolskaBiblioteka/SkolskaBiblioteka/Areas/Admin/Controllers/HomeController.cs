﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class HomeController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Home
        public ActionResult Index()
        {
            int na_citanju = db.Na_Citanju.Where(nc => nc.DatumVracanja == null).ToList().Count;
            int rezervacije = db.Rezervacije.Where(r => r.Validna).ToList().Count;
            int kasni = db.Na_Citanju.Where(nc => nc.DatumVracanja == null && nc.RokZaVracanje < DateTime.Now).ToList().Count;
            int prijave = db.Komentar.Where(k => k.Reported).ToList().Count;

            ViewBag.NaCitanju = na_citanju;
            ViewBag.Rezervacije = rezervacije;
            ViewBag.Kasni = kasni;
            ViewBag.Prijave = prijave;

            return View();
        }

        public ActionResult AutocompleteNaslov(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Knjiga.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Knjiga k in lista)
                    {
                        if (!Stringovi.Contains(k.Naziv))
                        {
                            Stringovi.Add(k.Naziv);
                        }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
        public ActionResult AutocompleteInv(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Primerak.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Primerak k in lista)
                    {
                        if (!Stringovi.Contains(k.InventarskiBroj))
                        {
                            Stringovi.Add(k.InventarskiBroj);
                        }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
        public ActionResult AutocompleteBarkod(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Primerak.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Primerak k in lista)
                    {
                        if (!Stringovi.Contains(k.BarCode))
                        {
                            if (!string.IsNullOrWhiteSpace(k.BarCode))
                                Stringovi.Add(k.BarCode);
                        }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
    }
}