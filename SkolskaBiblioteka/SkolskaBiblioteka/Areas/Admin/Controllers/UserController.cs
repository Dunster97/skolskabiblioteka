﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class UserController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/User
        public ActionResult Index(int? p)
        {
            var korisnici = db.Korisnik.OrderBy(k => k.KorisnikID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = korisnici.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(korisnici.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/User/Details/5
        public ActionResult Details(int id)
        {
            Korisnik kor = db.Korisnik.FirstOrDefault(k => k.KorisnikID == id);

            return View(kor);
        }

        // GET: Admin/User/Edit/5
        public ActionResult Edit(int id)
        {
            Korisnik kor = db.Korisnik.FirstOrDefault(k => k.KorisnikID == id);

            return View(kor);
        }

        // POST: Admin/User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Korisnik user)
        {
            try
            {
                Korisnik korisnik = db.Korisnik.FirstOrDefault(k => k.KorisnikID == id);
                if (korisnik != null)
                {
                    korisnik.MaticniBroj = user.MaticniBroj;
                    korisnik.Ime = user.Ime;
                    korisnik.Prezime = user.Prezime;
                    korisnik.Mesto = user.Mesto;
                    korisnik.Adresa = user.Adresa;
                    korisnik.Telefon = user.Telefon;
                    korisnik.ClanskaKartaBroj = user.ClanskaKartaBroj;
                    korisnik.BarCode = user.BarCode;
                    korisnik.Email = user.Email;
                    korisnik.Username = user.Username;
                    korisnik.LockedDateTime = user.LockedDateTime;
                    korisnik.LoginAttempt = user.LoginAttempt;
                    korisnik.PicPath = user.PicPath;

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/User/Delete/5
        public ActionResult Delete(int id)
        {
            Korisnik kor = db.Korisnik.FirstOrDefault(k => k.KorisnikID == id);

            return View(kor);
        }

        // POST: Admin/User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Korisnik korisnik = db.Korisnik.FirstOrDefault(k => k.KorisnikID == id);
                if(korisnik != null)
                {
                    db.Korisnik.Remove(korisnik);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
