﻿using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SkolskaBiblioteka.Areas.Admin.Controllers
{
    [AdminAuth]
    public class CommentController : Controller
    {
        SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

        // GET: Admin/Comment
        public ActionResult Index(int? p)
        {
            var komentari = db.Komentar.OrderByDescending(k => k.KomentarID);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = komentari.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(komentari.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        public ActionResult Reported(int? p)
        {
            var komentari = db.PrijavaKomentara.OrderByDescending(k => k.PrijavaKomentaraId);
            if (p <= 0) p = 1;

            ViewBag.PageSize = 10;
            ViewBag.PageNumber = (p ?? 1);
            ViewBag.PageCount = komentari.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).PageCount;
            ViewBag.HasPrevious = ((p ?? 1) != 1);
            ViewBag.HasNext = ((p ?? 1) != (int)ViewBag.PageCount);

            return View(komentari.ToPagedList((int)ViewBag.PageNumber, (int)ViewBag.PageSize).ToList());
        }

        // GET: Admin/Comment/Details/5
        public ActionResult Details(int id)
        {
            Komentar kom = db.Komentar.FirstOrDefault(k => k.KomentarID == id);

            return View(kom);
        }

        // GET: Admin/Comment/Edit/5
        public ActionResult Edit(int id)
        {
            Komentar kom = db.Komentar.FirstOrDefault(k => k.KomentarID == id);
            ViewBag.Korisnici = db.Korisnik.Select(f => new SelectListItem { Value = f.KorisnikID.ToString(), Text = f.Ime + " " + f.Prezime }).ToList();
            ViewBag.Knjige = db.Knjiga.Select(f => new SelectListItem { Value = f.KnjigaID.ToString(), Text = f.Naziv }).ToList();

            return View(kom);
        }

        // POST: Admin/Comment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Komentar kom)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Komentar komentar = db.Komentar.FirstOrDefault(k => k.KomentarID == id);
                    if(komentar != null)
                    {
                        komentar.KnjigaID = kom.KnjigaID;
                        komentar.KorisnikID = kom.KorisnikID;
                        komentar.Reported = kom.Reported;
                        komentar.KomentarText = kom.KomentarText;
                        komentar.VoteDown = kom.VoteDown;
                        komentar.VoteUp = kom.VoteUp;
                        komentar.VremeKomentara = kom.VremeKomentara;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Comment/Delete/5
        public ActionResult Delete(int id)
        {
            Komentar kom = db.Komentar.FirstOrDefault(k => k.KomentarID == id);

            return View(kom);
        }

        // POST: Admin/Comment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Komentar kom = db.Komentar.FirstOrDefault(k => k.KomentarID == id);
                if(kom != null)
                {
                    db.Komentar.Remove(kom);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
