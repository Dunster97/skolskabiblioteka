﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SkolskaBiblioteka
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "SkolskaBiblioteka.Controllers" }
            );

            routes.MapRoute(
                name: "ProfileView",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Profile", id = 0 }
            );

            routes.MapRoute(
                name: "KomentariPage",
                url: "{controller}/{action}/{knjigaId}/{p}",
                defaults: new { controller = "Library", action = "ChangePage", knjigaId = 0, p = 0 }
            );

            routes.MapRoute(
                name: "NotificationView",
                url: "{controller}/{action}/{id}/{ind}",
                defaults: new { controller = "Notification", action = "Delete", id = 0, ind = 0 }
            );

        }
    }
}
