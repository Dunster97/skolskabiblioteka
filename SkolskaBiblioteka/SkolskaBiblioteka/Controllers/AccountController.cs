﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkolskaBibliotekaDBContext;
using System.Text;
using System.Web.Security;
using SkolskaBiblioteka.ViewModels;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using SkolskaBiblioteka.Models;

namespace SkolskaBiblioteka.Controllers
{
    [LoadAuth]
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(Korisnik kor)
        {
            if (ModelState.IsValid)
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    try
                    {
                        bool unique = db.Korisnik.FirstOrDefault(k => k.Username.ToLower() == kor.Username.ToLower()) == null;
                        if (!unique) throw new Exception("Korisnicko ime vec postoji!");

                        bool unique2 = db.Korisnik.FirstOrDefault(k => k.MaticniBroj == kor.MaticniBroj) == null;
                        if (!unique2) throw new Exception("Maticni broj vec postoji!");

                        var user = db.Korisnik.Create();

                        user.Username = kor.Username.ToLower();
                        user.PasswordSalt = new SimpleCrypto.PBKDF2().GenerateSalt();
                        user.Password = new SimpleCrypto.PBKDF2().Compute(kor.Password, user.PasswordSalt);
                        user.Ime = kor.Ime;
                        user.Prezime = kor.Prezime;
                        user.Mesto = kor.Mesto;
                        user.Adresa = kor.Adresa;
                        user.Telefon = kor.Telefon;
                        user.Email = kor.Email;
                        user.MaticniBroj = kor.MaticniBroj;
                        user.ConfirmPassword = user.Password;
                        user.LockedDateTime = DateTime.Now.AddYears(-5);
                        user.PicPath = "/Images/avatar.png";

                        var korgrupa = db.KorisnikGrupa.Create();
                        korgrupa.KorisnikID = user.KorisnikID;
                        korgrupa.GrupaID = 1;
                        korgrupa.DatumDodavanja = DateTime.Now;

                        //TODO:SREDITI CLANSKAKRATABROJ
                        string a = db.Korisnik.ToList().Count.ToString("00000000");
                        a = a + DateTime.Now.ToString("yy");

                        user.ClanskaKartaBroj = a;

                        db.KorisnikGrupa.Add(korgrupa);
                        db.Korisnik.Add(user);
                        db.SaveChanges();

                        KorisnikZaPrikaz kzp = new KorisnikZaPrikaz(user);

                        return View("RegisterSuccess", kzp);
                    }
                    catch (Exception ex)
                    {
                        string exx = ex.Message;
                        ViewBag.Error = exx;
                        return View("_Error");
                    }
                }

            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult usernameIsUnique(string Username)
        {
            SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

            bool unique = db.Korisnik.FirstOrDefault(k => k.Username.ToLower() == Username.ToLower()) == null;
            return Json(unique);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult jmbgIsUnique(string MaticniBroj)
        {
            SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();

            bool unique = db.Korisnik.FirstOrDefault(k => k.MaticniBroj == MaticniBroj) == null;
            return Json(unique);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            ViewBag.Current = "Login";

            if (!Request.IsAuthenticated)
                return View();
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginUserModel kor, string ReturnUrl)
        {

            try
            {
                if (isValid(kor.Username, kor.Password))
                {
                    if (!isLocked(kor.Username))
                    {
                        using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                        {
                            var user = db.Korisnik.FirstOrDefault(x => x.Username == kor.Username);

                            KorisnikZaPrikaz kzp = new KorisnikZaPrikaz(user);

                            Session["Korisnik"] = kzp;

                            user.LatestVisit = DateTime.Now;

                            user.LoginAttempt = 0;
                            db.SaveChanges();
                        }

                        FormsAuthentication.SetAuthCookie(kor.Username, true);
                    }
                    else
                    {
                        //TODO: ERROR "Ovaj nalog je lockovan, pokusajte kasnije."
                        ViewBag.Error = "Овај налог је закључан, покушајте касније.";
                        return View("_Error");
                    }

                }
                else
                {
                    using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                    {
                        var user = db.Korisnik.FirstOrDefault(x => x.Username == kor.Username);

                        if (user != null)
                        {
                            if (!isLocked(user.Username))
                            {
                                if (user.LoginAttempt < Convert.ToInt32(Podesavanja.Get("BrojPokusaja")))
                                {
                                    user.LoginAttempt++;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    user.LockedDateTime = DateTime.Now;
                                    db.SaveChanges();
                                }
                                ViewBag.Error = "Корисничко име или лозинка нису валидни.";
                                return View("_Error");
                            }
                            else
                            {
                                ViewBag.Error = "Овај налог је закључан, покушајте касније.";
                                return View("_Error");
                            }
                        }
                        else
                        {
                            ViewBag.Error = "Корисничко име или лозинка нису валидни.";
                            return View("_Error");
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                string exx = ex.Message;
                ViewBag.Error = exx;
                return View("_Error");
            }

            if (ReturnUrl != null)
            {

                return Redirect(ReturnUrl);
            }
            else
                return RedirectToAction("Index", "Home");

        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["Korisnik"] = null;

            return RedirectToAction("Index", "Home");
        }

        private static bool isValid(string Username, string Password)
        {
            bool valid = false;

            SimpleCrypto.PBKDF2 crypto = new SimpleCrypto.PBKDF2();

            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var user = db.Korisnik.FirstOrDefault(x => x.Username == Username);

                if (user != null)
                    if (user.PasswordSalt != null)
                    {
                        if (user.Password == crypto.Compute(Password, user.PasswordSalt))
                            valid = true;
                    }
                    else
                    {
                        if (user.Password == Password)
                            valid = true;
                    }
            }

            return valid;
        }

        private static bool isLocked(string Username)
        {
            bool locked = true;

            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var user = db.Korisnik.FirstOrDefault(x => x.Username == Username);

                if ((DateTime.Now - user.LockedDateTime) > new TimeSpan(0, Convert.ToInt32(Podesavanja.Get("DuzinaZabrane")), 0))
                {
                    locked = false;
                }
            }

            return locked;
        }

        public new ActionResult Profile(int? id)
        {
            if (id != null)
            {
                try
                {
                    using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                    {
                        var user = db.Korisnik.FirstOrDefault(x => x.KorisnikID == id);

                        if (user != null)
                        {
                            ViewBag.NaCitanju = user.Na_Citanju.Count();
                            ViewBag.Rezervacije = user.Rezervacije.Count();
                            ViewBag.Preuzimanja = user.Preuzeo.Count();
                            ViewBag.Komentara = user.Komentar.Count();
                            return View(new KorisnikZaPrikaz(user));
                        }
                        else
                        //TODO: ADD ERROR 
                        {
                            ViewBag.Error = "Непостојећи корисник.";
                            return View("_Error");
                        }
                    }
                }
                catch (Exception ex)
                {
                    string exx = ex.Message;

                    ViewBag.Error = exx;
                    return View("_Error");

                }
            }
            else
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var user = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);
                    ViewBag.NaCitanju = user.Na_Citanju.Count();
                    ViewBag.Rezervacije = user.Rezervacije.Count();
                    ViewBag.Preuzimanja = user.Preuzeo.Count();
                    ViewBag.Komentara = user.Komentar.Count();
                    return View(new KorisnikZaPrikaz(user));
                }
            }
        }

        [HttpGet]
        public ActionResult Edit()
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var user = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                    KorisnikZaEdit k = new KorisnikZaEdit(user);

                    return View(k);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(KorisnikZaEdit k)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var user = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                    if (ModelState.IsValid)
                    {
                        if (isValid(user.Username, k.OldPassword))
                        {
                            if (k.NewPassword != null)
                            {
                                SimpleCrypto.PBKDF2 crypto = new SimpleCrypto.PBKDF2();
                                user.PasswordSalt = new SimpleCrypto.PBKDF2().GenerateSalt();
                                user.Password = new SimpleCrypto.PBKDF2().Compute(k.NewPassword, user.PasswordSalt);
                                user.ConfirmPassword = user.Password;
                            }
                            if (k.Mesto != "")
                                user.Mesto = k.Mesto;

                            if (k.Adresa != "")
                                user.Adresa = k.Adresa;

                            if (k.Telefon != "")
                                user.Telefon = k.Telefon;

                            if (k.Email != "")
                                user.Email = k.Email;

                            if (k.PicPath != null)
                                user.PicPath = "/Images/UserImages/" + k.PicPath;

                            db.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Тренутна лозинка није исправна. Уколико сте заборавили своју лозинку контактирајте администратора.";
                            return View("_Error");
                        }
                    }
                    else
                    {
                        ViewBag.Error = "Морате унети исправну лозинку.";
                        return View("_Error");
                    }


                    return View("Profile", new KorisnikZaPrikaz(user));
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;
                        var fileName = fileContent.FileName;
                        var picPath = Url.Content("/biblioteka/Images/UserImages/" + fileName);
                        if (fileName.EndsWith("jpg") || fileName.EndsWith("png") || fileName.EndsWith("gif"))
                        {
                            var path = Path.Combine(Server.MapPath("~/Images/UserImages"), fileName);
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                            }

                            return Json(picPath);
                        }
                        else
                        {
                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            return Json("Нисте одабрали слику. Подржани типови су: *.jpg, *.png, *.gif");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Дошло је до грешке. Покушај поново. Уколико се грешка понови обратите се техничкој подршци. " + ex.Message);
            }

            return Json("Успешно сте додали слику.");
        }

        public string GetNotificationCount()
        {
            if(User.Identity.IsAuthenticated)
            {
                if(Request.IsAuthenticated)
                {
                    SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel();
                    var user = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                    if(user != null)
                    {
                        return db.ObavestenjeKorisnik.Where(x => x.KorisnikID == user.KorisnikID && x.Seen != true).Count().ToString();
                    }
                }
            }

            return null;
        }
    }
}