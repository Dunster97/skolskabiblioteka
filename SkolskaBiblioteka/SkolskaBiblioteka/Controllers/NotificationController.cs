﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkolskaBibliotekaDBContext;
using SkolskaBiblioteka.ViewModels;
using SkolskaBiblioteka.Models;

namespace SkolskaBiblioteka.Controllers
{
    [LoadAuth]
    [Authorize]
    public class NotificationController : Controller
    {
        // GET: Notification
        [HttpGet]
        public ActionResult Index()
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var notifications = db.ObavestenjeKorisnik.Where(x => x.KorisnikID == korisnik.KorisnikID).ToList().OrderByDescending(x=>x.ObavestenjeKorisnikID);

                List<ObavestenjeZaPrikaz> OZP = new List<ObavestenjeZaPrikaz>();
                foreach (ObavestenjeKorisnik OK in notifications)
                {
                    OZP.Add(new ObavestenjeZaPrikaz(OK));
                }

                ViewBag.Current = "Notification";
                ViewBag.ind = 1;

                return View(OZP);
            }
        }

        [HttpPost]
        public ActionResult Details(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var notkor = db.ObavestenjeKorisnik.FirstOrDefault(x=> x.ObavestenjeKorisnikID == id && (x.KorisnikID == korisnik.KorisnikID||x.Obavestenje.Kreator.ToLower() == korisnik.Username.ToLower()));

                if (notkor != null)
                {

                    if (notkor.Seen != true && notkor.Obavestenje.Kreator.ToLower() != korisnik.Username.ToLower())
                    {
                        notkor.Seen = true;
                        notkor.SeenDate = DateTime.Now;
                    }

                    db.SaveChanges();

                    if (notkor.Obavestenje.Kreator.ToLower() == korisnik.Username.ToLower())
                        ViewBag.ind = 4;

                    return PartialView("_Details", new ObavestenjeZaPrikaz(notkor));
                }
                else
                {
                    ViewBag.Error = "Непостојеће обавештење.";
                    return PartialView("Error");
                }
            }    
        }

        [HttpPost]
        public ActionResult Delete(int id, int ind)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                    var notkor = db.ObavestenjeKorisnik.FirstOrDefault(x => x.KorisnikID == korisnik.KorisnikID && x.ObavestenjeKorisnikID == id);

                    db.ObavestenjeKorisnik.Remove(notkor);

                    db.SaveChanges();

                    if (ind == 1)
                        return AllNotifications();
                    if (ind == 2)
                        return NewNotifications();
                    if (ind == 3)
                        return SeenNotifications();
                    if (ind == 4)
                        return SentNotifications();
                    else
                        return AllNotifications();

                }
            }
            catch
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    if (ind == 1)
                        return AllNotifications();
                    if (ind == 2)
                        return NewNotifications();
                    if (ind == 3)
                        return SeenNotifications();
                    if (ind == 4)
                        return SentNotifications();
                    else
                        return AllNotifications();
                }
            }
        }

        [HttpPost]
        public ActionResult AllNotifications()
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var notifications = db.ObavestenjeKorisnik.Where(x => x.KorisnikID == korisnik.KorisnikID).ToList().OrderByDescending(x => x.ObavestenjeKorisnikID);

                List<ObavestenjeZaPrikaz> OZP = new List<ObavestenjeZaPrikaz>();
                foreach (ObavestenjeKorisnik OK in notifications)
                {
                    OZP.Add(new ObavestenjeZaPrikaz(OK));
                }

                ViewBag.ind = 1;

                return PartialView("_Obavestenja", OZP);
            }
        }

        [HttpPost]
        public ActionResult NewNotifications()
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var notifications = db.ObavestenjeKorisnik.Where(x => x.KorisnikID == korisnik.KorisnikID && x.Seen != true).ToList().OrderByDescending(x => x.ObavestenjeKorisnikID);

                List<ObavestenjeZaPrikaz> OZP = new List<ObavestenjeZaPrikaz>();
                foreach (ObavestenjeKorisnik OK in notifications)
                {
                    OZP.Add(new ObavestenjeZaPrikaz(OK));
                }

                ViewBag.ind = 2;

                return PartialView("_Obavestenja", OZP);
            }
        }

        [HttpPost]
        public ActionResult SeenNotifications()
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var notifications = db.ObavestenjeKorisnik.Where(x => x.KorisnikID == korisnik.KorisnikID && x.Seen == true).ToList().OrderByDescending(x => x.ObavestenjeKorisnikID);

                List<ObavestenjeZaPrikaz> OZP = new List<ObavestenjeZaPrikaz>();
                foreach (ObavestenjeKorisnik OK in notifications)
                {
                    OZP.Add(new ObavestenjeZaPrikaz(OK));
                }

                ViewBag.ind = 3;

                return PartialView("_Obavestenja", OZP);
            }
        }

        [HttpPost]
        public ActionResult SentNotifications()
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var obavestenja = db.Obavestenje.Where(x => x.Kreator.ToLower() == korisnik.Username.ToLower()).ToList().OrderByDescending(x => x.ObavestenjeID);

                List<ObavestenjeZaPrikaz> OZP = new List<ObavestenjeZaPrikaz>();
                foreach (Obavestenje o in obavestenja)
                {
                    var obavestenjek = db.ObavestenjeKorisnik.FirstOrDefault(x => x.ObavestenjeID == o.ObavestenjeID);
                    if(obavestenjek != null)
                        OZP.Add(new ObavestenjeZaPrikaz(obavestenjek));
                }

                ViewBag.ind = 4;

                return PartialView("_Obavestenja", OZP);
            }
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Obavestenje o)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    o.Kreator = User.Identity.Name;
                    o.PostDate = DateTime.Now;
                    db.Obavestenje.Add(o);
                    db.SaveChanges();

                    ObavestenjeKorisnik ok = db.ObavestenjeKorisnik.Create();
                    ok.ObavestenjeID = o.ObavestenjeID;
                    ok.KorisnikID = db.Korisnik.FirstOrDefault(x => x.Username.ToLower() == "admin".ToLower()).KorisnikID;
                    ok.Seen = false;
                    ok.SeenDate = DateTime.Now;
                    db.ObavestenjeKorisnik.Add(ok);
                    db.SaveChanges();

                    return View("ContactSuccess");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
    }
}