﻿using SkolskaBiblioteka.Models;
using SkolskaBiblioteka.ViewModels;
using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SkolskaBiblioteka.Controllers
{
    [LoadAuth]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var kor = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);
                    if (kor.LatestVisit != null)
                    {
                        if ((DateTime.Now - kor.LatestVisit).Value.Minutes > 10)
                        {
                            kor.LatestVisit = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        kor.LatestVisit = DateTime.Now;
                        db.SaveChanges();
                    }
                }

            IndexZaPrikaz IZP = new IndexZaPrikaz();

            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                db.Komentar.OrderByDescending(x => x.VoteUp - x.VoteDown).Take(2).ToList().ForEach(x => IZP.Komentari.Add(new KomentarZaPrikaz(x, true)));
                db.Knjiga.OrderByDescending(x => x.KnjigaID).Take(4).ToList().ForEach(x => IZP.Knjige.Add(new KnjigaZaPrikaz(x)));
            }

            ViewBag.Current = "Index";

            return View(IZP);
        }

        public ActionResult About()
        {
            ViewBag.Current = "About";

            return View();
        }
    }
}