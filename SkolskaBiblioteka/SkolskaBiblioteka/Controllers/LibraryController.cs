﻿using SkolskaBiblioteka.Models;
using SkolskaBiblioteka.ViewModels;
using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SkolskaBiblioteka.Controllers
{
    [LoadAuth]
    [Authorize]
    public class LibraryController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            return View(new SkolskaBibliotekaDBModel().Knjiga.ToList());
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Book(int? id)
        {
            if (id != null)
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == id);

                    if (book != null)
                    {

                        KnjigaDetails kd = new KnjigaDetails(book, 0);
                        kd.NoviKomentar = new Komentar();
                        kd.NoviKomentar.KnjigaID = book.KnjigaID;

                        return View(kd);
                    }
                    else
                    {
                        ViewBag.Error = "Страница није пронађена.";
                        return View("_Error");
                    }
                }
            }
            else
            {
                ViewBag.Error = "Страница није пронађена.";
                return View("_Error");
            }
        }

        [HttpPost]
        public ActionResult Book(Komentar k)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                int korisnikId = db.Korisnik.FirstOrDefault(y => y.Username == User.Identity.Name).KorisnikID;
                k.Knjiga = db.Knjiga.FirstOrDefault(x => x.KnjigaID == k.KnjigaID);
                k.VremeKomentara = DateTime.Now;
                k.KorisnikID = korisnikId;
                k.VoteUp = 0;
                k.VoteDown = 0;
                k.Reported = false;

                db.Komentar.Add(k);
                db.SaveChanges();

                KnjigaDetails kd = new KnjigaDetails(k.Knjiga, 0);

                return PartialView("_Komentari", kd);
            }
        }

        [HttpPost]
        public ActionResult RemoveComment(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var kom = db.Komentar.FirstOrDefault(x => x.KomentarID == id);

                KorisnikZaPrikaz kzp = (KorisnikZaPrikaz)Session["Korisnik"];

                if (kom != null)
                {
                    if (kom.KorisnikID == kzp.KorisnikId || kzp.AccessLevels.Min() == 7)
                    {
                        var book = kom.Knjiga;

                        db.Komentar.Remove(kom);

                        KnjigaDetails kd = new KnjigaDetails(book, 0);

                        db.SaveChanges();

                        return PartialView("_Komentari", kd);
                    }
                    else
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return new EmptyResult();
                    }
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return new EmptyResult();
                }
            }
        }

        [HttpPost]
        public ActionResult Reserve(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var rez = db.Rezervacije.Create();

                rez.KnjigaID = (int)id;
                rez.Knjiga = db.Knjiga.FirstOrDefault(x => x.KnjigaID == rez.KnjigaID);
                rez.KorisnikID = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name).KorisnikID;

                var primerak = db.Primerak.FirstOrDefault(x => x.Aktivna && x.KnjigaID == rez.KnjigaID);

                if (primerak != null)
                {
                    rez.PrimerakID = primerak.PrimerakID;

                    rez.Validna = true;

                    primerak.Aktivna = false;

                    rez.DatumPocetkaRezervacije = DateTime.Now;
                    rez.DatumIstekaRezervacije = DateTime.Now.AddDays(Convert.ToDouble(Podesavanja.Get("DuzinaRezervacije")));

                    db.Rezervacije.Add(rez);

                    if (ModelState.IsValid)
                        db.SaveChanges();

                    var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == id);

                    KnjigaDetails kd = new KnjigaDetails(book, 0);
                    kd.NoviKomentar = new Komentar();
                    kd.NoviKomentar.KnjigaID = book.KnjigaID;

                    return PartialView("_CommentInfo", kd);
                }
                else
                {
                    db.Rezervacije.Add(rez);

                    //DB Tasks:Dodati dnevno proveravanje knjiga i dodati valid = true ako je rezervisana knjiga vracena i datume pocetka i isteka rezervacije
                    //Ili proveravati rezervacije pri svakom vracanju knjige ili otkazu rezervacije.

                    rez.DatumPocetkaRezervacije = DateTime.Now;
                    rez.DatumIstekaRezervacije = DateTime.Now.AddDays(Convert.ToDouble(Podesavanja.Get("DuzinaRezervacijeBezKnjige")));

                    rez.Validna = true;

                    if (ModelState.IsValid)
                        db.SaveChanges();

                    var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == id);

                    KnjigaDetails kd = new KnjigaDetails(book, 0);
                    kd.NoviKomentar = new Komentar();
                    kd.NoviKomentar.KnjigaID = book.KnjigaID;

                    return PartialView("_CommentInfo", kd);
                }

            }
        }

        [HttpPost]
        public ActionResult RemoveReserve(int id)
        {

            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                int korisnikId = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name).KorisnikID;

                var rezervacija = db.Rezervacije.FirstOrDefault(x => x.KorisnikID == korisnikId && x.KnjigaID == id && x.Validna == true);

                if (rezervacija != null)
                {
                    if (rezervacija.PrimerakID != null)
                    {
                        var primerak = db.Primerak.FirstOrDefault(x => x.PrimerakID == rezervacija.PrimerakID);
                        primerak.Aktivna = true;
                    }

                    db.Rezervacije.Remove(rezervacija);

                    var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == id);

                    KnjigaDetails kd = new KnjigaDetails(book, 0);
                    kd.NoviKomentar = new Komentar();
                    kd.NoviKomentar.KnjigaID = book.KnjigaID;

                    db.SaveChanges();

                    return PartialView("_CommentInfo", kd);
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return new EmptyResult();
                }
            }
        }

        [HttpPost]
        public ActionResult Download(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                var preuzeo = db.Preuzeo.FirstOrDefault(x => x.KorisnikID == korisnik.KorisnikID && x.KnjigaID == id);

                var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == id);

                if (preuzeo == null && book != null)
                {
                    preuzeo = db.Preuzeo.Create();
                    preuzeo.KnjigaID = id;
                    preuzeo.Knjiga = book;
                    preuzeo.Korisnik = korisnik;
                    preuzeo.KorisnikID = korisnik.KorisnikID;
                    preuzeo.VremePreuzimanja = DateTime.Now;

                    db.Preuzeo.Add(preuzeo);

                    db.SaveChanges();
                }

                if (book != null)
                {

                    KnjigaDetails kd = new KnjigaDetails(book, 0);
                    kd.NoviKomentar = new Komentar();
                    kd.NoviKomentar.KnjigaID = book.KnjigaID;

                    return PartialView("_CommentInfo", kd);
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return new EmptyResult();
                }
            }
        }

        [HttpPost]
        public ActionResult VoteUp(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {

                var vu = db.VoteUp.Create();
                vu.KomentarID = id;
                vu.KorisnikID = ((KorisnikZaPrikaz)Session["Korisnik"]).KorisnikId;
                int korid = (int)vu.KorisnikID;

                var Komentar = db.Komentar.FirstOrDefault(x => x.KomentarID == id);
                if (db.VoteUp.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == vu.KorisnikID) == null)
                {
                    if (db.VoteDown.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid) != null)
                    {
                        VoteDown v = db.VoteDown.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid);
                        db.VoteDown.Remove(v);
                        db.SaveChanges();
                        Komentar.VoteDown = db.VoteDown.Where(x => x.KomentarID == id).Count();
                    }
                    db.VoteUp.Add(vu);
                    db.SaveChanges();
                    Komentar.VoteUp = db.VoteUp.Where(x => x.KomentarID == id).Count();
                    db.SaveChanges();

                }
                else
                {
                    if (db.VoteUp.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == vu.KorisnikID) != null)
                    {
                        VoteUp v = db.VoteUp.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == vu.KorisnikID);
                        db.VoteUp.Remove(v);
                        db.SaveChanges();
                        Komentar.VoteUp = db.VoteUp.Where(x => x.KomentarID == id).Count();
                        db.SaveChanges();
                    }
                }

                KomentarZaPrikaz kzp = new KomentarZaPrikaz(Komentar, true);



                return PartialView("_Komentar", kzp);
            }
        }

        [HttpPost]
        public ActionResult VoteDown(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var vd = db.VoteDown.Create();
                vd.KomentarID = id;
                vd.KorisnikID = ((KorisnikZaPrikaz)Session["Korisnik"]).KorisnikId;

                int korid = (int)vd.KorisnikID;

                var Komentar = db.Komentar.FirstOrDefault(x => x.KomentarID == id);

                if (db.VoteDown.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == vd.KorisnikID) == null)
                {
                    if (db.VoteUp.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid) != null)
                    {
                        VoteUp v = db.VoteUp.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid);
                        db.VoteUp.Remove(v);
                        db.SaveChanges();
                        Komentar.VoteUp = db.VoteUp.Where(x => x.KomentarID == id).Count();
                    }
                    db.VoteDown.Add(vd);
                    db.SaveChanges();
                    Komentar.VoteDown = db.VoteDown.Where(x => x.KomentarID == id).Count();
                    db.SaveChanges();

                }
                else
                {
                    if (db.VoteDown.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid) != null)
                    {
                        VoteDown v = db.VoteDown.FirstOrDefault(x => x.KomentarID == id && x.KorisnikID == korid);
                        db.VoteDown.Remove(v);
                        db.SaveChanges();
                        Komentar.VoteDown = db.VoteDown.Where(x => x.KomentarID == id).Count();
                        db.SaveChanges();
                    }
                }

                KomentarZaPrikaz kzp = new KomentarZaPrikaz(Komentar, true);


                return PartialView("_Komentar", kzp);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ChangePage(int knjigaId, int p)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var book = db.Knjiga.FirstOrDefault(x => x.KnjigaID == knjigaId);
                KnjigaDetails kd = new KnjigaDetails(book, p);
                kd.NoviKomentar = new Komentar();
                kd.NoviKomentar.KnjigaID = book.KnjigaID;

                return PartialView("_Komentari", kd);
            }
        }

        [HttpGet]
        public ActionResult Report(int id)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                ReportZaPrikaz RZP = new ReportZaPrikaz();

                var komentar = db.Komentar.FirstOrDefault(x => x.KomentarID == id);

                if (komentar != null)
                {

                    var korisnik = db.Korisnik.FirstOrDefault(x => x.Username == User.Identity.Name);

                    var report = db.PrijavaKomentara.FirstOrDefault(x => x.KomentarId == id && x.KorisnikId == korisnik.KorisnikID);

                    if (report == null)
                    {

                        KomentarZaPrikaz kzp = new KomentarZaPrikaz(komentar, false);
                        RZP.kzp = kzp;

                        return PartialView("Report", RZP);
                    }
                    else
                    {
                        ViewBag.Error = "Већ сте пријавили овај коментар.";
                        return PartialView("_Error");
                    }
                }
                else
                {
                    ViewBag.Error = "Непостојећи коментар.";
                    return PartialView("_Error");
                }
            }
        }

        [HttpPost]
        public ActionResult Report(ReportZaPrikaz RZP, int KomentarId)
        {
            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
            {
                var report = db.PrijavaKomentara.Create();
                report.KomentarId = KomentarId;
                report.Tekst = RZP.report.Tekst;
                db.Komentar.FirstOrDefault(x => x.KomentarID == KomentarId).Reported = true;
                report.KorisnikId = ((KorisnikZaPrikaz)Session["Korisnik"]).KorisnikId;
                report.VremePrijave = DateTime.Now;

                db.PrijavaKomentara.Add(report);
                db.SaveChanges();

                return PartialView("ReportSuccess");
            }
        }

    }
}