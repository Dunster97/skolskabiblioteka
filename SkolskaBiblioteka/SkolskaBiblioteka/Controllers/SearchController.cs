﻿using SkolskaBiblioteka.ViewModels;
using SkolskaBibliotekaDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SkolskaBiblioteka.Models;

namespace SkolskaBiblioteka.Controllers
{
    [LoadAuth]
    public class SearchController : Controller
    {
        // GET: Search
        [HttpGet]
        public ActionResult Index(string sortOrder, int? page, string NameString, string AutorString, string IzdavacString, string ZanrString, int? GodinaInt, string Izdanje)
        {
            try
            {

                ViewBag.Naslov = NameString;
                ViewBag.ImeAutora = AutorString;
                ViewBag.ImeIzdavaca = IzdavacString;
                ViewBag.Signatura = ZanrString;
                ViewBag.PapirnoIzdanje = Izdanje;
                ViewBag.GodinaIzanja = GodinaInt;


                if (String.IsNullOrEmpty(sortOrder))
                {
                    sortOrder = "Name";
                }
                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = sortOrder == "Name" ? "name_desc" : "Name";
                ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
                ViewBag.IzdavacSortParm = sortOrder == "Izdavac" ? "izdavac_desc" : "Izdavac";
                ViewBag.ZanrSortParm = sortOrder == "Zanr" ? "zanr_desc" : "Zanr";
                ViewBag.PapirSortParm = sortOrder == "Papir" ? "papir_desc" : "Papir";

                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var knjige = from s in db.Knjiga
                                 select s;
                    switch (sortOrder)
                    {
                        case "Name":
                            knjige = knjige.OrderBy(x => x.Naziv);
                            break;
                        case "name_desc":
                            knjige = knjige.OrderByDescending(s => s.Naziv);
                            break;
                        case "Date":
                            knjige = knjige.OrderBy(s => s.DatumIzdavanja);
                            break;
                        case "date_desc":
                            knjige = knjige.OrderByDescending(s => s.DatumIzdavanja);
                            break;
                        case "Izdavac":
                            knjige = knjige.OrderBy(s => s.Izdavac.Naziv);
                            break;
                        case "izdavac_desc":
                            knjige = knjige.OrderByDescending(s => s.Izdavac.Naziv);
                            break;
                        case "Zanr":
                            knjige = knjige.OrderBy(s => s.Signatura.Naziv);
                            break;
                        case "zanr_desc":
                            knjige = knjige.OrderByDescending(s => s.Signatura.Naziv);
                            break;
                        case "Papir":
                            knjige = knjige.OrderBy(s => s.PapirnoIzdanje);
                            break;
                        case "papir_desc":
                            knjige = knjige.OrderByDescending(s => s.PapirnoIzdanje);
                            break;
                    }


                    if (!String.IsNullOrWhiteSpace(NameString))
                    {
                        knjige = knjige.Where(x => x.Naziv.ToUpper().Contains(NameString.ToUpper()));
                    }
                    if (!String.IsNullOrWhiteSpace(IzdavacString))
                    {
                        knjige = knjige.Where(x => x.Izdavac.Naziv.ToUpper().Contains(IzdavacString.ToUpper()));
                    }
                    if (!String.IsNullOrWhiteSpace(ZanrString))
                    {
                        knjige = knjige.Where(x => x.Signatura.Naziv.ToUpper().Contains(ZanrString.ToUpper()));
                    }

                    if (Izdanje == "Papirno")
                    {
                        knjige = knjige.Where(x => x.PapirnoIzdanje == true);
                    }
                    else
                    {
                        if (Izdanje == "Elektronsko")
                        {
                            knjige = knjige.Where(x => x.PapirnoIzdanje == false);
                        }
                    }

                    if (GodinaInt != null)
                    {
                        if (ViewBag.GodinaIzanja > 0)
                            knjige = knjige.Where(x => x.DatumIzdavanja.Value.Year == GodinaInt);
                    }

                    var Knjige = knjige.ToList();

                    if (!String.IsNullOrWhiteSpace(AutorString))
                    {

                        Knjige = Knjige.Where(x => Contains(x.Autor.ToList(), AutorString.ToUpper())).ToList();
                    }


                    List<KnjigaZaPrikaz> KnjigeZaPrikaz = new List<KnjigaZaPrikaz>();

                    //if (Knjige.Count != 0)
                    //    foreach (Knjiga k in Knjige)
                    //    {
                    //        if (k != null)
                    //            KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
                    //    }

                    Knjige.ForEach(x => { if (x != null) KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(x)); });

                    int pageSize = 20;
                    int pageNumber = (page ?? 1);



                    return View(KnjigeZaPrikaz.ToPagedList(pageNumber, pageSize));
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }

        public ActionResult Search(string sortOrder, int? page, string NameString, string AutorString, string IzdavacString, string ZanrString, int? GodinaInt, string Izdanje)
        {
            try
            {

                ViewBag.Naslov = NameString;
                ViewBag.ImeAutora = AutorString;
                ViewBag.ImeIzdavaca = IzdavacString;
                ViewBag.Signatura = ZanrString;
                ViewBag.PapirnoIzdanje = Izdanje;
                ViewBag.GodinaIzanja = GodinaInt;


                if (String.IsNullOrEmpty(sortOrder))
                {
                    sortOrder = "Name";
                }
                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = sortOrder == "Name" ? "name_desc" : "Name";
                ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
                ViewBag.IzdavacSortParm = sortOrder == "Izdavac" ? "izdavac_desc" : "Izdavac";
                ViewBag.ZanrSortParm = sortOrder == "Zanr" ? "zanr_desc" : "Zanr";
                ViewBag.PapirSortParm = sortOrder == "Papir" ? "papir_desc" : "Papir";

                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var knjige = from s in db.Knjiga
                                 select s;
                    switch (sortOrder)
                    {
                        case "Name":
                            knjige = knjige.OrderBy(x => x.Naziv);
                            break;
                        case "name_desc":
                            knjige = knjige.OrderByDescending(s => s.Naziv);
                            break;
                        case "Date":
                            knjige = knjige.OrderBy(s => s.DatumIzdavanja);
                            break;
                        case "date_desc":
                            knjige = knjige.OrderByDescending(s => s.DatumIzdavanja);
                            break;
                        case "Izdavac":
                            knjige = knjige.OrderBy(s => s.Izdavac.Naziv);
                            break;
                        case "izdavac_desc":
                            knjige = knjige.OrderByDescending(s => s.Izdavac.Naziv);
                            break;
                        case "Zanr":
                            knjige = knjige.OrderBy(s => s.Signatura.Naziv);
                            break;
                        case "zanr_desc":
                            knjige = knjige.OrderByDescending(s => s.Signatura.Naziv);
                            break;
                        case "Papir":
                            knjige = knjige.OrderBy(s => s.PapirnoIzdanje);
                            break;
                        case "papir_desc":
                            knjige = knjige.OrderByDescending(s => s.PapirnoIzdanje);
                            break;
                    }


                    if (!String.IsNullOrWhiteSpace(NameString))
                    {
                        knjige = knjige.Where(x => x.Naziv.ToUpper().Contains(NameString.ToUpper()));
                    }
                    if (!String.IsNullOrWhiteSpace(IzdavacString))
                    {
                        knjige = knjige.Where(x => x.Izdavac.Naziv.ToUpper().Contains(IzdavacString.ToUpper()));
                    }
                    if (!String.IsNullOrWhiteSpace(ZanrString))
                    {
                        knjige = knjige.Where(x => x.Signatura.Naziv.ToUpper().Contains(ZanrString.ToUpper()));
                    }

                    if (Izdanje == "Papirno")
                    {
                        knjige = knjige.Where(x => x.PapirnoIzdanje == true);
                    }
                    else
                    {
                        if (Izdanje == "Elektronsko")
                        {
                            knjige = knjige.Where(x => x.PapirnoIzdanje == false);
                        }
                    }

                    if (GodinaInt != null)
                    {
                        if (ViewBag.GodinaIzanja > 0)
                            knjige = knjige.Where(x => x.DatumIzdavanja.Value.Year == GodinaInt);
                    }

                    var Knjige = knjige.ToList();

                    if (!String.IsNullOrWhiteSpace(AutorString))
                    {

                        Knjige = Knjige.Where(x => Contains(x.Autor.ToList(), AutorString.ToUpper())).ToList();
                    }


                    List<KnjigaZaPrikaz> KnjigeZaPrikaz = new List<KnjigaZaPrikaz>();

                    //if (Knjige.Count != 0)

                    Knjige.ForEach(x => { if (x != null) KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(x)); });

                    //foreach (Knjiga k in Knjige)
                    //{
                    //    if (k != null)
                    //        KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
                    //}

                    int pageSize = 20;
                    int pageNumber = (page ?? 1);



                    return PartialView("_Search", KnjigeZaPrikaz.ToPagedList(pageNumber, pageSize));
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }

        //[AllowAnonymous]
        //[HttpPost]
        //public ActionResult Index(SearchParams parametri)
        //{
        //    ViewBag.Current = "Search";
        //    //TODO: Zameni KnjigeZaPrikaz sa Knjige
        //    try
        //    {
        //        List<Knjiga> Knjige = new List<Knjiga>();
        //        List<KnjigaZaPrikaz> KnjigeZaPrikaz = new List<KnjigaZaPrikaz>();
        //        if (parametri != null)
        //            using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
        //            {
        //                if (parametri.Naslov != null)
        //                {
        //                    Knjige = db.Knjiga.Where(x => x.Naziv.ToUpper().Contains(parametri.Naslov.ToUpper())).OrderBy(x => x.Naziv).ToList();
        //                    if (Knjige.Count != 0)
        //                        foreach (Knjiga k in Knjige)
        //                        {
        //                            KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
        //                        }
        //                }
        //                else
        //                {
        //                    Knjige = db.Knjiga.ToList().OrderBy(x => x.Naziv).Take(10).ToList();
        //                    if (Knjige.Count != 0)
        //                        foreach (Knjiga k in Knjige)
        //                        {
        //                            if (k != null)
        //                                KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
        //                        }
        //                }
        //            }
        //        int? page = 1;
        //        int pageSize = 20;
        //        int pageNumber = (page ?? 1);

        //        return View(KnjigeZaPrikaz.ToPagedList(pageNumber, pageSize));
        //    }
        //    catch (Exception ex)
        //    {
        //        string ErrorMessage = ex.Message;
        //        ViewBag.Error = ErrorMessage;
        //        return View("_Error");
        //    }

        //}

        //[AllowAnonymous]
        //[HttpPost]
        //public ActionResult Advanced(SearchParams parametri)
        //{
        //    ViewBag.Current = "Search";

        //    try
        //    {

        //        List<KnjigaZaPrikaz> KnjigeZaPrikaz = new List<KnjigaZaPrikaz>();
        //        using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
        //        {
        //            var Knjige = db.Knjiga.ToList();
        //            if (parametri != null)
        //            {
        //                if (parametri.Naslov != null)
        //                {
        //                    Knjige = Knjige.Where(x => x.Naziv.ToUpper().Contains(parametri.Naslov.ToUpper())).ToList();
        //                }
        //                if (parametri.ImeIzdavaca != null)
        //                {
        //                    Knjige = Knjige.Where(x => x.Izdavac.Naziv.ToUpper().Contains(parametri.ImeIzdavaca.ToUpper())).ToList();
        //                }
        //                if (parametri.Signatura != null)
        //                {
        //                    Knjige = Knjige.Where(x => x.Signatura.Naziv.ToUpper().Contains(parametri.Signatura.ToUpper())).ToList();
        //                }
        //                if (parametri.ImeAutora != null)
        //                {
        //                    Knjige = Knjige.Where(x => Contains(x.Autor.ToList(), parametri.ImeAutora)).ToList();
        //                }
        //                if (parametri.PapirnoIzdanje != null)
        //                {
        //                    Knjige = Knjige.Where(x => x.PapirnoIzdanje == parametri.PapirnoIzdanje).ToList();
        //                }

        //                if (parametri.GodinaIzanja != null)
        //                {
        //                    if (parametri.GodinaIzanja > 0)
        //                        Knjige = Knjige.Where(x => x.DatumIzdavanja.Value.Year == parametri.GodinaIzanja).ToList();
        //                }

        //                if (Knjige.Count != 0)
        //                    foreach (Knjiga k in Knjige)
        //                    {
        //                        KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
        //                    }
        //            }
        //            else
        //            {
        //                Knjige = db.Knjiga.ToList().OrderBy(x => x.Naziv).Take(10).ToList();
        //                if (Knjige.Count != 0)
        //                    foreach (Knjiga k in Knjige)
        //                    {
        //                        if (k != null)
        //                            KnjigeZaPrikaz.Add(new KnjigaZaPrikaz(k));
        //                    }
        //            }
        //        }
        //        return View("Index", KnjigeZaPrikaz.Take(10).ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        string ErrorMessage = ex.Message;
        //        ViewBag.Error = ErrorMessage;
        //        return View("_Error");
        //    }
        //}

        private static bool Contains(List<Autor> Lista, string Naziv)
        {
            try
            {
                foreach (Autor a in Lista)
                {
                    string s = a.Ime + " " + a.Prezime;
                    if (s.Contains(Naziv))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ActionResult AutocompleteNaslov(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Knjiga.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Knjiga k in lista)
                    {
                        if (!Stringovi.Contains(k.Naziv))
                        {
                            Stringovi.Add(k.Naziv);
                        }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }

        public ActionResult AutocompleteAutor(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Autor.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Autor a in lista)
                    {
                        if (a.Ime != null && a.Prezime != null)
                            if (!Stringovi.Contains(a.Ime + " " + a.Prezime))
                            {
                                Stringovi.Add(a.Ime + " " + a.Prezime);
                            }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
        public ActionResult AutocompleteIzdavac(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Izdavac.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Izdavac i in lista)
                    {
                        if (i.Naziv != null)
                            if (!Stringovi.Contains(i.Naziv))
                            {
                                Stringovi.Add(i.Naziv);
                            }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
        public ActionResult AutocompleteSignatura(string term)
        {
            try
            {
                using (SkolskaBibliotekaDBModel db = new SkolskaBibliotekaDBModel())
                {
                    var lista = db.Signatura.ToList();
                    List<string> Stringovi = new List<string>();

                    foreach (Signatura s in lista)
                    {
                        if (s.Naziv != null)
                            if (!Stringovi.Contains(s.Naziv))
                            {
                                Stringovi.Add(s.Naziv);
                            }
                    }
                    var filteredItems = Stringovi.Where(
                        item => item.ToLower().StartsWith(term.ToLower())
                        ).Take(10);
                    return Json(filteredItems, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("_Error");
            }
        }
    }
}