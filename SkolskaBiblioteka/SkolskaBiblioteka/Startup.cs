﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SkolskaBiblioteka.Startup))]
namespace SkolskaBiblioteka
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            ConfigureAuth(app);
        }
    }
}
